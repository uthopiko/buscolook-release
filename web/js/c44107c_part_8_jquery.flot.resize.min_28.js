/* Flot plugin for automatically redrawing plots as the placeholder resizes.

Copyright (c) 2007-2013 IOLA and Ole Laursen.
Licensed under the MIT license.

It works by listening for changes on the placeholder div (through the jQuery
resize event plugin) - if the size changes, it will redraw the plot.

There are no options. If you need to disable the plugin for some plots, you
can just fix the size of their placeholders.

*/
(function(b){b.plot.plugins.push({init:function(a){function c(){var d=a.getPlaceholder();0!=d.width()&&0!=d.height()&&(a.resize(),a.setupGrid(),a.draw())}a.hooks.bindEvents.push(function(a,b){a.getPlaceholder().resize(c)});a.hooks.shutdown.push(function(a,b){a.getPlaceholder().unbind("resize",c)})},options:{},name:"resize",version:"1.0"})})(jQuery);