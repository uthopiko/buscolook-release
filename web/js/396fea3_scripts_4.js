var BUSCOLOOK;

BUSCOLOOK = {

    mobile: false,

    init: function () {
        this.setup();
        this.menu.init();
        //this.filters.init()
        this.filterIcons.init();
        //this.filterMenu.init();
        this.galleries.init();
        this.tabs.init();
        this.profileTabs.init();
        this.buscolookers.init();
        this.likes.init();
        this.togglers.init();
        this.autocomplete.init();
        this.dynamic_selects.init();
        this.popups.init();
    },

    setup: function () {
        var html = $("html");
        if (html.hasClass("mobile") || html.hasClass("ipod") || html.hasClass("ipad") || html.hasClass("iphone")) {
            this.mobile = true;
        }

        /*$(".inputSelectWrap select").customSelect({
            customClass: "inputSelect"
        });*/



        $(window).load(function () {
            var setSquare = function () {
                $(".squareContainer").each(function () {
                    $(this).height($(this).width());
                });
            };

            setSquare();

            $(window).on("resize", function () {
                setSquare();
            });


            if ($("#coverHeader").length) {
                $("#coverHeader").css("height", $("#main > .profileContentHeader").offset().top - $("#topBar").height());
            }
        });
    }
};

BUSCOLOOK.menu = {

    init: function () {
        this.menu = $("nav#menu");
        this.trigger = $("#header").find(".menuTrigger");

        this.markSubmenus();
        this.firstSubmenuHover();
        this.checkables.init();

        this.navLikeMenus();

        this.mobile();
    },

    markSubmenus: function () {
        this.menu.find("li").each(function () {
            var item = $(this);
            if (item.find("ul").length > 0) {
                item.addClass("hasChild");
                if (!item.find("> a").data("clickable")) {
                    item.find("> a").click(function (e) {
                        e.preventDefault();
                    });
                }
            }

            if (item.find("ul li").length == 1) {
                item.addClass("childHasOneItem");
            }
        });
    },

    firstSubmenuHover: function () {
        this.menu.find("li").hover(function () {
            var element = $(this);
            var parent = element.parent();
            element.addClass("hover");
//			parent.addClass("hover");
            if (element.index() == 0) {
                parent.addClass("firstChildHover");
            }
        }, function () {
            var element = $(this);
            var parent = element.parent();
            element.removeClass("hover");
            if (element.index() == 0) {
                parent.removeClass("firstChildHover");
            }
        });
    },

    navLikeMenus: function () {
        if ($(".navLikeMenu").length > 0) {
            $(".navLikeMenu > li").hover(function () {
                var element = $(this);
                var parent = element.parent();
                element.addClass("hover");
                if (element.index() == 0) {
                    parent.addClass("firstChildHover");
                }
            }, function () {
                var element = $(this);
                var parent = element.parent();
                element.removeClass("hover");
                if (element.index() == 0) {
                    parent.removeClass("firstChildHover");
                }
            });
        }
    },

    mobile: function () {
        var _this = this;
        this.trigger.on("click", function () {
            if (_this.menu.data("open")) {
                _this.menu.removeClass("open");
                _this.menu.data("open", false);
            } else {
                _this.menu.addClass("open");
                _this.menu.data("open", true);
            }
        });
    }

};

BUSCOLOOK.filterIcons = {
    init: function () {
        this.resetZone = $(".remove_zona_filter");
        this.resetCategories = $(".remove_category_filter");

        this.resetZone.on("click",function(e) {
            var template = 'buscolook_web_cotilleo';
            if (window.location.href.indexOf('/tienda/') != -1){
                template = 'buscolook_web_punto_venta';
            }

            var url = Routing.generate(template, {'zona': 'buscolook','type':'en','category':sessionStorage.category});
            window.location = url;
        });
        this.resetCategories.on("click",function(e) {
            var template = 'buscolook_web_cotilleo';
            if (window.location.href.indexOf('/tienda/') != -1){
                template = 'buscolook_web_punto_venta';
            }

            var url = Routing.generate(template, {'zona': sessionStorage.zona,'type':'en','category':'ropa'});
            window.location = url;
        });

    }
};

BUSCOLOOK.galleries = {

    carousel_config: {
        items: 1,
        autoHeight: true,
        singleItem: true,
        pagination: false,
        responsive: true,
        autoPlay: true,
        lazyLoad: true,
        navigation: false
    },

    banner_carousel_config: {
        items: 1,
        autoHeight: true,
        singleItem: true,
        pagination: false,
        responsive: true,
        autoPlay: true,
        lazyLoad: false,
        navigation: false
    },

    init: function () {
        var _this = this;

        if (BUSCOLOOK.mobile) {
            this.carousel_config.autoPlay = false;
        }

        this.carousels = $(".owl-carousel").each(function () {
            var slides = $(this);
            var preview = slides.parents(".preview");
            var owl = slides.owlCarousel(_this.carousel_config);

            if (slides.find(".slide").length > 1 && preview) {
                var btn_prev = $('<span class="galleryPrev"></span>');
                var btn_next = $('<span class="galleryNext"></span>');
                btn_prev.on("click", function (e) {
                    owl.trigger("owl.prev");
                    e.stopPropagation();
                });
                btn_next.on("click", function (e) {
                    owl.trigger("owl.next");
                    e.stopPropagation();
                });

                if (slides.parents(".gallery").find(".like").length > 0) {
                    slides.append(btn_prev);
                    slides.append(btn_next);

                } else {
                    preview.append(btn_prev);
                    preview.append(btn_next);
                }
            }
        });

        this.hpBanner();
        this.bigGallery();

        if (!BUSCOLOOK.mobile) {
            this.clickThroughs();
        }
    },

    hpBanner: function () {
        var banner_slides = $(".banner-owl-carousel");
        var banner = banner_slides.parents(".banner");
        var banner_carousel = banner_slides.owlCarousel(this.banner_carousel_config);

        var btn_prev = $('<span class="galleryPrev"></span>');
        var btn_next = $('<span class="galleryNext"></span>');

        btn_prev.on("click", function (e) {
            banner_carousel.trigger("owl.prev");
            e.stopPropagation();
        });
        btn_next.on("click", function (e) {
            banner_carousel.trigger("owl.next");
            e.stopPropagation();
        });

        banner.append(btn_prev);
        banner.append(btn_next);
    },

    bigGallery: function () {


        var btn_prev = $('<span class="galleryPrev"></span>');
        var btn_next = $('<span class="galleryNext"></span>');

        var big_gallery_config = this.carousel_config;

        if ($(".galleryThumbs").length > 0) {
            big_gallery_config.afterMove = after_move_function;
        }

        var carousel = $(".owl-carousel-big").owlCarousel(big_gallery_config);

        btn_prev.on("click", function (e) {
            carousel.trigger("owl.prev");
            e.stopPropagation();
        });
        btn_next.on("click", function (e) {
            carousel.trigger("owl.next");
            e.stopPropagation();
        });

        carousel.append(btn_prev);
        carousel.append(btn_next);

        if ($(".galleryThumbs").length > 0) {
            var thumbs = $(".galleryThumbs");
            var owl_carousel = carousel.data("owlCarousel");
            thumbs.find("a").on("click", function (e) {
                var thumb = $(this);
                var slide = thumb.data("slide");
                owl_carousel.goTo(slide);
                e.preventDefault();
            });

            big_gallery_config.afterMove = function () {

            };

            function after_move_function() {
                var thumbs = $(".galleryThumbs");
                var gallery_current_item = carousel.data("owlCarousel").currentItem;
                var thumb_item = thumbs.find("a[data-slide='" + gallery_current_item + "']");
                thumbs.find("a").removeClass("active");
                thumb_item.addClass("active");
            }
        }
    },

    clickThroughs: function () {
        $(".grid .item, .banner .item").each(function () {
            var item = $(this);
            item.find(".like").on("click", function (e) {
                e.stopPropagation();
            });
            if (item.find("*[data-href]")) {
                var href = item.find("*[data-href]").data("href");
                if (href) {
                    var clicker = item.find(".gallery");
                    clicker.addClass("clickable");
                    if (href == "same") {
                        href = item.find("a[data-href]").attr("href");
                    }
                    clicker.on("click", function (e) {
                        window.location = href;
                    });
                    if (item.find(".heading .subtitle").length > 0) {
                        var subtitle = item.find(".heading .subtitle");
                        subtitle.addClass("clickable");
                        subtitle.on("click", function (e) {
                            window.location = href;
                        });
                    }
                }
            }
        });
    }
};

BUSCOLOOK.menu.checkables = {

    init: function () {
        this._this = BUSCOLOOK.menu;
        this.onload();
        this.bind();
    },

    onload: function () {
        this._this.menu.find("a[data-checkable=true]").each(function () {
            var item = $(this);
            var parent = item.parent();
            if (item.data("checked")) {
                parent.addClass("checked");
                item.addClass("checked");
            }
        });
    },

    bind: function () {
        this._this.menu.find("a[data-checkable=true]").click(function (e) {
            var item = $(this);
            var parent = item.parent();
            if (item.data("checked")) {
                parent.removeClass("checked");
                item.removeClass("checked")
                item.data("checked", false);
            } else {
                /*parent.addClass("checked");
                 item.addClass("checked")
                 item.data("checked", true);*/
                window.location = item.attr('href');
            }
            e.preventDefault();
        });
    }
};

BUSCOLOOK.tabs = {

    init: function () {
        this.bind();
    },

    bind: function () {
        $(".tabs").each(function () {
            var container = $(this);
            var nav = container.find(".tabsNav");
            var links = nav.find("a");
            var tabs = container.find(".tab");

            links.on("click", function (e) {
                var link = $(this);
                var id = link.attr("href").replace("#", "");
                if ($("#" + id).length > 0) {
                    var tab = $("#" + id);
                    links.removeClass("active");
                    link.addClass("active");
                    tabs.hide();
                    tab.fadeIn(500);
                    e.preventDefault();
                }
            });
        });
    }
};

BUSCOLOOK.togglers = {

    init: function () {
        this.dataAttributes = {
            connectId: "data-connect-id"
        };

        this.bind();
        this.userDefined();
    },

    bind: function () {
        var selector = ".toggler";
        var content = ".content";
        var openClass = "open";
        var _this = this;
        var isActive = false;
        $(selector + ":not(." + openClass + ")").find(content).hide();
        $(selector).each(function () {
            $(this).find(".heading").on("click", function (e) {
                e.preventDefault();
                if ($(this).parent().hasClass(openClass)) {
                    $(this).siblings(content).slideUp().parent().removeClass(openClass);
                }
                else {
                    $(this).siblings(content).slideDown().parent().addClass(openClass);
                }
            });
        });
    },
    userDefined: function () {
        var selector = ".slideToggle";
        var openClass = "open";
        var _this = this;
        var isActive = false;
        $(selector + ":not(." + openClass + ")").each(function () {
            $("#" + $(this).attr(_this.dataAttributes.connectId)).css("display", "none");
        });
        $(selector).on("click", function (e) {
            e.preventDefault();
            $(this).toggleClass(openClass);
            isActive = $(this).hasClass(openClass);
            var node = $("#" + $(this).attr(_this.dataAttributes.connectId));
            if (node) {
                if (isActive) {
                    $(node).slideDown();
                }
                else {
                    $(node).slideUp();
                }
            }
        });
    }
};

BUSCOLOOK.profileTabs = {

    init: function () {
        this.bind();
    },

    bind: function () {
        $(".cf .item").each(function () {
            var container = $(this);

            container.on("click", function (e) {
                $('.tabsContent').css('display', 'none');
                $('#' + this.id + 'Content').css('display', 'block');
                $('.cf li').removeClass('active');
                container.addClass('active');
                e.preventDefault();
            });
        });
    }
};


BUSCOLOOK.buscolookers = {

    init: function () {
        $(".buscolookers .item .button.following, .profileContentHeader a.following, .profileContent a.following").each(function () {
            var button = $(this);
            var text_default = button.html();
            var text_alt = button.data("alt-text");

            button.hover(function () {
                button.html(text_alt);
            }, function () {
                button.html(text_default);
            });
        });
    }
};

BUSCOLOOK.popups = {
    init: function () {
        $("a.popup").magnificPopup({
            type: 'inline',
            preloader: false,
            focus: '#myPopup',

            // Disables focusing on form elements for some mobiles
            callbacks: {
                beforeOpen: function () {
                    if ($(window).width() < 700) {
                        this.st.focus = false;
                    } else {
                        this.st.focus = '#myPopup';
                    }
                },
                open: function () {
                    var _this = BUSCOLOOK.galleries;
                    var bussinessId = readCookie('bussinessId');
                    var buscolookerId = readCookie('buscolookerId');
                    if (bussinessId != null) {
                        var url = Routing.generate('buscolook_web_fichaje_bussiness_create', {'bussinessId': bussinessId});
                    }
                    else {
                        var url = Routing.generate('buscolook_web_fichaje_buscolooker_create', {'buscolooker': buscolookerId});
                    }
                    $.ajax({
                        url: url,
                        type: 'POST',
                        success: function (data) {
                            if (bussinessId != null) {
                                var urlGarment = Routing.generate('buscolook_web_bussiness_garment', {'fichajeId': data.fichajeId, 'bussinessId': bussinessId});
                                var urlPublish = Routing.generate('buscolook_web_publish_fichaje', {'fichajeId': data.fichajeId, 'type': 'buscolooker'});
                            }
                            else {
                                var urlGarment = Routing.generate('buscolook_web_bussiness_garment_buscolooker', {'fichajeId': data.fichajeId, 'buscolooker': buscolookerId});
                                var urlPublish = Routing.generate('buscolook_web_publish_fichaje', {'fichajeId': data.fichajeId, 'type': 'bussiness'});
                            }
                            $("#form-garments").attr('action', urlGarment);
                            $("#form-file-input").attr('action', Routing.generate('buscolook_web_fichaje_add_image', {'fichajeId': data.fichajeId }));
                            $("#publish_fichaje").attr('href', urlPublish);

                        }
                    });
                    BUSCOLOOK.setup();

                    //$(".owl-carousel").owlCarousel(_this.carousel_config);
                    //$(".owl-carousel").data('owlCarousel').reinit();
                }
            }
        });

        $("a.popup-fichaje-details").magnificPopup({
            type: 'ajax',
            preloader: false,


            // Disables focusing on form elements for some mobiles
            callbacks: {
                beforeOpen: function () {
                    /*if($(window).width() < 700) {
                     this.st.focus = false;
                     } else {
                     this.st.focus = '#myPopup';
                     }*/
                },
                ajaxContentAdded: function () {
                    var _this = BUSCOLOOK.galleries;
                    var bussinessId = readCookie('bussinessId');

                    BUSCOLOOK.setup();
                    BUSCOLOOK.togglers.init();
                    $(".inputSelectWrap select").customSelect({
                        customClass: "inputSelect"
                    });
                    BUSCOLOOK.galleries.init();
                }
            }
        });
    }
};

BUSCOLOOK.likes = {

    init: function () {
        $(".icon-like").each(function () {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_favourite_add_fichaje', {'fichaje': item});

            button.on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'POST'
                }).done(function (data) {
                    button.removeClass('icon-like');
                    button.addClass('icon-like-full');
                    var spanCount = button.next();
                    spanCount.text(parseInt(spanCount.text()) + 1);
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-full").each(function () {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_favourite_remove_fichaje', {'fichaje': item});

            button.on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'DELETE'
                }).done(function (data) {
                    button.removeClass('icon-like-full');
                    button.addClass('icon-like');
                    var spanCount = button.next();
                    spanCount.text(parseInt(spanCount.text()) - 1);
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-black").each(function () {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_follower_add_shop', {'bussinessId': item});

            button.on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'POST'
                }).done(function (data) {
                    button.removeClass('icon-like-black');
                    button.addClass('icon-like-black-full');
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-black-full").each(function () {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_follower_remove_shop', {'bussinessId': item});

            button.on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'DELETE'
                }).done(function (data) {
                    button.removeClass('icon-like-black-full');
                    button.addClass('icon-like-black');
                    BUSCOLOOK.likes.init();
                });
            });
        });
    }
};

BUSCOLOOK.dynamic_selects = {
    init: function() {
        $('.principal-selector').on('change',function(e) {
            var url = $(this).data('url');
            var id = $(this).val();
            url = Routing.generate(url,{'bussiness':id});
            $.ajax({
                url: url,
                type: 'GET'
            }).done(function(data){
                $('.secondary-selector')
                    .find('option')
                    .remove()
                    .end();
                $.each(data,function (i, item) {
                    $('.secondary-selector')
                        .append('<option value="'+item.id+'">'+item.name+'</option>');
                });
                $('.secondary-selector').closest('div').css('display','block');

            })
        })
    }
};

BUSCOLOOK.autocomplete =  {
    init: function() {
        $('.autocomplete').on('keypress',function(e) {
            var input = $(this);
            var value = input.val();

            if (value.length > 3) {
                alert(value);
            }
            e.preventDefault();
        });
    }
};


$(document).ready(function () {
    BUSCOLOOK.init();
});

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}