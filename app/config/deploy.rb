set :application, "Buscolook"
set :domain,      "46.105.16.228"
set :deploy_to,   "/var/www/blk"
set :app_path,    "app"

set :repository,  "git@bitbucket.org:uthopiko/buscolook-release.git"
set :scm,         :git
set :branch,      "master"

role :web,        domain
role :app,        domain
role :db,         domain, :primary => true

set :use_composer, true
set :install_vendors, true

set :keep_releases,  6

set :dump_assetic_assets, true
set :clear_controllers, false


set :user,        "uthopiko"
set :use_sudo, false
#set :shared_children, [app_path + "/logs", app_path + "/cache", web_path + "/uploads", app_path + "/config/sphinx/indexes", "vendor"]
set :shared_children, [app_path + "/logs", app_path + "/cache", web_path + "/uploads", "vendor"]
set :writable_dirs, [app_path + "/cache", app_path + "/logs", app_path + "/config/sphinx/indexes"]
set :shared_files,    ["app/config/parameters.yml"]
# set :webserver_user,      "uthopiko"
# set :permission_method,   :chown
# set :use_set_permissions, true
set :interactive_mode, false

#before "symfony:cache:warmup", "symfony:doctrine:migrations:migrate"

before 'symfony:cache:warmup' do
  run "php -r 'apc_clear_cache();'"
  run "cd /var/www/blk/shared/app && rm -rf cache/* && rm -rf logs/*"
end

after "deploy", "symfony:assets:install"

#after 'deploy' do
#  run "varnishadm -T 127.0.0.1:6082 -S /etc/varnish/secret 'ban req.url ~ /'"
#end

logger.level = Logger::MAX_LEVEL