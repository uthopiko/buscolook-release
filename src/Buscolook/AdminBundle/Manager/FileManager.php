<?php
namespace Buscolook\AdminBundle\Manager;

use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Buscolook\WebBundle\Entity\BannerMetadata;
use Buscolook\WebBundle\Entity\EntityImage;
use Buscolook\WebBundle\Utils\Tools;
use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class FileManager
{

    private $uploadType;
    private $amazonConfig = [];
    private $client;
    private $em;
    private $tools;
    private $dataManager;
    private $filterManager;
    private $rootDir;
    private $filters;

    const AWS = 1;
    const LOCAL = 2;

    public function __construct($type, EntityManager $em, array $amazonConfig, Tools $tools, Request $request, Session $session, DataManager $dataManager, FilterManager $filterManager, $rootDir, $filters)
    {
        $this->uploadType = $type;
        $this->amazonConfig = $amazonConfig;
        $this->em = $em;
        $this->utils = $tools;
        $this->request = $request;
        $this->filterManager = $filterManager;
        $this->dataManager = $dataManager;
        $this->rootDir = $rootDir;
        $this->filters = $filters;
        $this->session = $session;

        $this->client = S3Client::factory(array(
            'key' => $this->amazonConfig[0],
            'secret' => $this->amazonConfig[1],
        ));

    }

    public function uploadFile($files, $entity, $entityType, $filename = null)
    {
        $folder = $entityType . '/' . $entity->getId() . '/';
        foreach ($files as $file) {
            if (is_array($file) || !strpos(get_class($file), 'UploadedFile')) {
                $file = $file[0];
            }

            $thumbnails = $this->createThumbnails($file, $this->filters[strtolower($this->utils->getRealClass($entity)) . '.filters'], $folder);
            if ($this->uploadType === self::AWS) {
                $result = $this->awsMultipleUpload($thumbnails, $folder);
                $result = $result->getAll();
            } else {
                $result = $this->localMultipleUpload($thumbnails,$folder);
            }
            $result['fileName'] = $thumbnails['normal']['name'];
            $result['name'] = $filename;

            $this->insertIntoDatabase(get_class($entity), $entity, $result);
            return $result;
        }
    }

    public function uploadFileBanner($files, $entity, $entityType, $filename = null)
    {
        $folder = $entityType . '/' . $entity->getId() . '/';
        foreach ($files as $file) {
            $newFile = $file[0]->move('uploads/documents', uniqid() . '.jpg');

            if ($this->uploadType === self::AWS) {
                $result = $this->awsUpload($newFile, $folder);
                $result = $result->getAll();
                $result['fileName'] = $newFile->getFilename();
                $result['name'] = $filename;
            } else {

            }

            $this->insertIntoDatabase(get_class($entity), $entity, $result);
            return $result;
        }
    }

    public function deleteFile($filename, $name = null, $inspirationId = null)
    {

        if ($this->uploadType === self::AWS) {
            $this->awsDelete($filename);
            return true;
        } else {

        }
    }


    protected function createThumbnails(UploadedFile $file, $filters, $folder)
    {

        $tmpFolderPathAbs = $this->rootDir . '/../web/uploads/tmp/'; // folder to store unfiltered temp file
        $tmpImageNameNoExt = uniqid();
        $tmpImageName = $tmpImageNameNoExt . '.' . $file->getClientOriginalExtension();

        $file->move($tmpFolderPathAbs, $tmpImageName);
        $tmpImagePathRel = '/uploads/tmp/' . $tmpImageName;
        // Create the filtered image:


        $permanentFolderPath = $folder . 'normal' . '/';
        $permanentImagePath = $permanentFolderPath . $tmpImageName;

        $files ['normal'] = ['name'=>$tmpImageName, 'filename' => $permanentImagePath,'content'=> file_get_contents($tmpFolderPathAbs.$tmpImageName)];
        foreach ($filters as $filter) {
            $processedImage = $this->dataManager->find($filter, $tmpImagePathRel);
            $filteredBinary = $this->filterManager->applyFilter($processedImage, $filter);
            $filteredImage = $filteredBinary->getContent();

            $permanentFolderPath = $folder . $filter . '/';
            $permanentImagePath = $permanentFolderPath . $tmpImageName;

            $files [$filter] = ['name'=>$tmpImageName, 'filename' => $permanentImagePath,'content'=> $filteredImage];
        }
        unlink($tmpFolderPathAbs . $tmpImageName); // eliminate unfiltered temp file.
        return $files;
    }

    private function awsUpload(File $file, $folder)
    {
        $result = $this->client->putObject(array(
            'Bucket' => $this->amazonConfig[2],
            'Key' => $folder . $file->getFilename(),
            'SourceFile' => $file->getPathname()
        ));

        return $result;
    }

    private function awsMultipleUpload($files, $folder)
    {
        foreach ($files as $key => $file) {
        $result [$key]= $this->client->putObject(array(
            'Bucket' => $this->amazonConfig[2],
            'Key' => $file['filename'],
            'Body' => $file['content']
        ));
    }

        return $result['normal'];
    }

    /**
     * @param $folder
     */
    protected function localMultipleUpload($files, $folder)
    {
        $url = [];
        foreach ($files as $key => $file) {
            $url [] = '/uploads/' .$file['filename'];
            if (!is_dir(dirname($this->rootDir . '/../web/uploads/' .$file['filename']))) {
                mkdir(dirname($this->rootDir . '/../web/uploads/' .$file['filename']), 0777, true);
            }
            $fp = fopen($this->rootDir . '/../web/uploads/' .$file['filename'], 'w+');
            fwrite($fp, $file['content']);
            fclose($fp);
        }
        $result['ObjectURL'] = $url[0];
        return $result;
    }

    private function awsDelete($filename)
    {
        $result = $this->client->deleteObject(array(
            // Bucket is required
            'Bucket' => $this->amazonConfig[2],
            // Key is required
            'Key' => $filename
        ));

        return $result;
    }


    private function localUpload()
    {

    }

    private function insertIntoDatabase($entityType, $entity, $result)
    {
        $entity2 = $this->utils->getRealClass($entity);
        if ($entity2 == "Banner") {
            $link = $this->session->get('link');
            $imageEntity = new BannerMetadata();
            $imageEntity->setBanner($entity);
            $imageEntity->setImage($result['ObjectURL']);
            $imageEntity->setSort(1);
            $imageEntity->setLink($link);

        } else {
            $entityWithoutClass = $this->utils->getRealClass($entity);
            $entityImageString = '\\' . $entityType . 'Image';

            $imageEntity = new $entityImageString;
            $imageEntity->setImage($result['fileName']);
            $setter = 'set' . $entityWithoutClass;
            $imageEntity->$setter($entity);
            $imageEntity->setName($result['name']);
            $imageEntity->setPrincipal(true);

        }
        $this->em->persist($imageEntity);
        $this->em->flush();

    }

    public function deleteFromDatabase($whereClause, $entityRepositoryName)
    {
        $imageEntity = $this->em->getRepository($entityRepositoryName)->findOneBy($whereClause);
        $this->em->remove($imageEntity);
        $this->em->flush();

    }
}