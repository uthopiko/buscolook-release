<?php
namespace Buscolook\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SettingController extends Controller
{
    public function indexAction()
    {
        return $this->render('BuscolookAdminBundle:Settings:settings.html.twig');
    }
}
