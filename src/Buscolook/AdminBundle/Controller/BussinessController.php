<?php

namespace Buscolook\AdminBundle\Controller;

use Buscolook\AdminBundle\Form\BussinessImageType;
use Buscolook\AdminBundle\Form\BussinessType;
use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Entity\BussinessImage;
use Buscolook\WebBundle\Form\BussinessProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class BussinessController extends Controller
{
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BuscolookWebBundle:Bussiness')->find($id);
        $shops = $em->getRepository('BuscolookWebBundle:Shop')->findByBussiness($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $images = $em->getRepository('BuscolookWebBundle:BussinessImage')->findBy(['bussiness' => $id]);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'shops' => $shops,
                'images' => $images
            ]
        );
    }

    public function addImageAction(Request $request, Bussiness $bussiness)
    {
        $image = new BussinessImage();
        $imageForm = $this->createForm(new BussinessImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $filename = $request->request->get('filename');
            $file = $request->files->get('files');
            $arrKey = $file[0]->getClientOriginalName();
            $size = $file[0]->getClientSize();
            $mimeType = $file[0]->getMimeType();
            $fileReturn = $uploadManager->uploadFile($request->files, $bussiness, 'tiendas', $filename[$arrKey]);
            $response = new JsonResponse();
            $array = ['files' => [['url' => $fileReturn['ObjectURL'],
                'thumbnail_url' => $fileReturn['ObjectURL'],
                'name' => $filename[$arrKey],
                'type' => $mimeType,
                'size' => $size,
                'delete_url' => $this->generateUrl('buscolook_admin_bussiness_delete_image', ['id'=>$bussiness->getId(),'filename'=>$fileReturn['fileName']]),
                'delete_type' => 'POST']]];
            $response->setData($array);

            return $response;
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function deleteImageAction(Request $request, Bussiness $bussiness, $filename)
    {
        $image = new BussinessImage();
        $imageForm = $this->createForm(new BussinessImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('tiendas/'. $bussiness->getId() . '/' . $filename);
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new BussinessProfileType(), new Bussiness());
        $form->submit($request);

        if ($form->isValid()) {
            $form->getData()->setUser($em->getRepository('BuscolookWebBundle:User')->find(1));
            $em->persist($form->getData());
            $em->flush();

            $return = [];
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function getViewPath()
    {
        return 'Bussiness';
    }

    public function getEntityClass()
    {
        return new Bussiness();
    }

    public function getEntityType()
    {
        return new BussinessProfileType();
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }
}
