<?php
/**
 * Created by PhpStorm.
 * User: aramos
 * Date: 12/24/13
 * Time: 4:41 PM
 */

namespace Buscolook\AdminBundle\Controller;


use Buscolook\AdminBundle\Form\InspirationImageType;
use Buscolook\WebBundle\Entity\Inspiration;
use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\AdminBundle\Form\GarmentImageType;
use Buscolook\WebBundle\Entity\EntityImage;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\WebBundle\Entity\GarmentImage;
use Buscolook\AdminBundle\Form\InspirationType;
use Buscolook\WebBundle\Entity\InspirationImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class InspirationController extends BaseCrudController
{

    public function addImageAction(Request $request, Inspiration $inspiration)
    {
        $image = new InspirationImage();
        $imageForm = $this->createForm(new InspirationImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $filename = $request->request->get('filename');
            $file = $request->files->get('files');
            $arrKey = $file[0]->getClientOriginalName();
            $size = $file[0]->getClientSize();
            $mimeType = $file[0]->getMimeType();
            $fileReturn = $uploadManager->uploadFile($file, $inspiration, 'inspiration', $filename[$arrKey]);
            $response = new JsonResponse();
            $array = ['files' => [['url' => $fileReturn['ObjectURL'],
                'thumbnail_url' => $fileReturn['ObjectURL'],
                'name' => $filename[$arrKey],
                'type' => $mimeType,
                'size' => $size,
                'delete_url' => $this->generateUrl('buscolook_admin_inspiration_delete_image', ['id'=>$inspiration->getId(),'filename'=>$filename[$arrKey]]),
                'delete_type' => 'POST']]];
            $response->setData($array);

            return $response;
        }
        return $this->render('BuscolookAdminBundle:Image:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function deleteImageAction(Request $request, Inspiration $inspiration, $filename)
    {
        $image = new InspirationImage();
        $imageForm = $this->createForm(new InspirationImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('inspiration/'. $inspiration->getId() . '/' . $filename);
            $uploadManager->deleteFromDatabase(['name' => $filename, 'inspiration' => $inspiration->getId()], 'BuscolookWebBundle:InspirationImage');
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getEntityRepository()->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $images = $em->getRepository('BuscolookWebBundle:InspirationImage')->findBy(['inspiration' => $id]);


        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'images' => $images
            ]
        );
    }

    public function getViewPath()
    {
        return 'Inspiration';
    }


    public function getEntityClass()
    {
        return new Inspiration();
    }

    public function getEntityType()
    {
        return new InspirationType();
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Inspiration');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }
} 