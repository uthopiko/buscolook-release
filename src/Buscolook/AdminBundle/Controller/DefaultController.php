<?php

namespace Buscolook\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Null;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BuscolookAdminBundle:Default:index.html.twig', array('name' => 'Hola'));
    }

    public function navbarAction()
    {
        $sc = $this->container->get('security.context');
        if (!$sc->isGranted('ROLE_ADMIN') && $sc->isGranted('ROLE_BUSSINESS')) {
            $user = $sc->getToken()->getUser();
            $bussiness = $this->getDoctrine()->getRepository('BuscolookWebBundle:Bussiness')->findOneBy(['user'=>$user->getId()]);
            return $this->render('BuscolookAdminBundle::navbar.html.twig', array('bussiness_id' => $bussiness->getId()));
        } else {
            return $this->render('BuscolookAdminBundle::navbar.html.twig', array('bussiness_id' => null));
        }
    }
}
