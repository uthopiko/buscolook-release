<?php

namespace Buscolook\AdminBundle\Controller\Crud;

use Symfony\Component\HttpFoundation\Response;

interface CrudControllerInterface
{
    /**
     * Gets the view path
     * @return mixed
     */
    function getViewPath();

    /**
     * Gets the entity class
     * @return mixed
     */
    function getEntityClass();

    /**
     * Gets the form type
     * @return mixed
     */
    function getEntityType();

    /**
     * Gets the entity repository class
     * @return mixed
     */
    function getEntityRepository();

    /**
     * List action
     * @return Response
     */
    function indexAction();

    /**
     * New element action
     * @return Response
     */
    function addAction();

    /**
     * Edit element action
     * @param int $entityId
     * @return Response
     */
    function editAction($entityId);
}