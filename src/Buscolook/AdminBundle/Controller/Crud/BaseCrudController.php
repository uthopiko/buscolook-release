<?php

namespace Buscolook\AdminBundle\Controller\Crud;

use Symfony\Component\HttpFoundation\Request;
use Buscolook\WebBundle\Controller\BaseController;
use Buscolook\AdminBundle\Controller\Crud\CrudControllerInterface;

abstract class BaseCrudController extends BaseController implements CrudControllerInterface
{
    abstract public function getViewPath();

    abstract public function getEntityClass();

    abstract public function getEntityType();

    abstract public function getEntityRepository();

    public function indexAction()
    {
        $items = $this->getAll();
        return $this->render(sprintf('BuscolookAdminBundle:%s:index.html.twig', $this->getViewPath()),
            [
                'items' => $items
            ]);
    }

    public function addAction()
    {
        $form = $this->createForm($this->getEntityType(), $this->getEntityClass());
        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView()
            ]
        );
    }

    public function editAction($id)
    {
        $entity = $this->getEntityRepository()->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity
            ]
        );
    }

    public function editTabImageAction(Request $request, $parentId, $id)
    {
        $entity = $this->getEntityRepository()->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $images = $this->getRepository('EntityImage')->findBy(['entityId' => $id, 'entityType' => $this->getViewPath() ]);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'images' => $images,
                'parentId' => $parentId
            ]
        );
    }

    public function createAction(Request $request)
    {
        $em = $this->getEntityManager();
        $form = $this->createForm($this->getEntityType(), $this->getEntityClass());
        $form->submit($request);

        if ($form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            $return = [];
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function updateAction($id, Request $request)
    {
        $entity = $this->getEntityRepository()->find($id);

        $em = $this->getEntityManager();

        $form = $this->createForm($this->getEntityType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('buscolook_admin_' . strtolower($this->getViewPath()) . '_edit', ['id' => $id]));
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function deleteAction($id)
    {
        $entity = $this->getEntityRepository()->find($id);

        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();

        return [];
    }

    public function subIndexAction($parentId)
    {
        $items = $this->getAll();
        return $this->render(
            sprintf(
                'BuscolookAdminBundle:%s:index.html.twig',
                $this->getViewPath()
            ),
            [
                'items' => $items,
                'parentId' => $parentId
            ]
        );
    }

    public function subAddAction($parentId)
    {
        $form = $this->createForm($this->getEntityType(), $this->getEntityClass());
        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'parentId' => $parentId
            ]
        );
    }

    public function subEditAction($parentId, $id)
    {
        $entity = $this->getEntityRepository()->find($id);

        $form = $this->createForm($this->getEntityType(), $entity);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'parentId' => $parentId
            ]
        );
    }

    public function subCreateAction(Request $request, $parentId)
    {

        $em = $this->getEntityManager();
        $form = $this->createForm($this->getEntityType(), $this->getEntityClass());
        $form->submit($request);

        if ($form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            $return = [];
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function subUpdateAction($parentId, $id, Request $request)
    {
        $entity = $this->getEntityRepository()->find($id);

        $em = $this->getEntityManager();

        $form = $this->createForm($this->getEntityType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('buscolook_admin_' . strtolower($this->getViewPath()) . '_edit', ['id' => $id]));
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function subDeleteAction($parentId, $id)
    {
        $entity = $this->getEntityRepository()->find($id);

        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();

        return [];
    }
}