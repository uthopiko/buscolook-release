<?php

namespace Buscolook\AdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ImageController extends Controller
{
    public function editAction(Request $request, $imageType, $id)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('BuscolookAdminBundle:Image:edit.html.twig');
    }
}