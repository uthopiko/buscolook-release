<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 10/25/13
 * Time: 9:16 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\AdminBundle\Controller;


use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\AdminBundle\Form\BannerMetadataType;
use Buscolook\AdminBundle\Form\BannerType;
use Buscolook\WebBundle\Entity\Banner;
use Buscolook\WebBundle\Entity\BannerMetadata;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BannerController extends BaseCrudController
{
    public function getViewPath()
    {
        return 'Banner';
    }


    public function getEntityClass()
    {
        return new Banner();
    }

    public function getEntityType()
    {
        return new BannerType();
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Banner');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }

    public function editAction($id)
    {
        $entity = $this->getEntityRepository()->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $images = $this->getRepository('BannerMetadata')->findBy(['banner' => $id]);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'images' => $images
            ]
        );
    }

    public function addImageAction(Request $request, Banner $banner)
    {
        $em = $this->getDoctrine()->getManager();
        $image = new BannerMetadata();
        $imageForm = $this->createForm(new BannerMetadataType(), $image);

        if ($request->getMethod() === 'POST') {
            $imageForm->handleRequest($request);

            if ($imageForm->isValid()) {
                $uploadManager = $this->get('buscolook.upload.files');

                $fileRequest = $imageForm->getData('image');
                $session = $this->get('session');
                $session->set('link',$fileRequest->getLink());
                $file[0] = $fileRequest->getImage();
                $filename = $arrKey = $file[0]->getClientOriginalName();
                $size = $file[0]->getClientSize();
                $mimeType = $file[0]->getMimeType();
                $fileReturn = $uploadManager->uploadFile($file, $banner, 'banner', $filename);
                $response = new JsonResponse();
                $array = ['files' => [['url' => $fileReturn['ObjectURL'],
                    'thumbnail_url' => $fileReturn['ObjectURL'],
                    'name' => $filename,
                    'type' => $mimeType,
                    'size' => $size,
                    'delete_url' => '#',//$this->generateUrl('buscolook_admin_bussiness_delete_image', ['id' => $bussiness->getId(), 'filename' => $fileReturn['fileName']]),
                    'delete_type' => 'POST']]];
                $response->setData($array);

                return $response;
            }
        }
        return $this->render('BuscolookAdminBundle:Banner:images.html.twig', ['form' => $imageForm->createView(), 'banner' => $banner]);
    }

    public function deleteImageAction(Request $request, BannerMetadata $bannerMetadata)
    {
        /*$image = new BannerMetadata();
        $imageForm = $this->createForm(new BussinessImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('tiendas/' . $bussiness->getId() . '/' . $filename);
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));*/
        $id = $bannerMetadata->getBanner()->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($bannerMetadata);
        $em->flush();

        return $this->redirect($this->generateUrl('buscolook_admin_banner_edit',array('id'=>$id)));
    }


}