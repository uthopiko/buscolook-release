<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 10/16/13
 * Time: 9:34 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\AdminBundle\Controller;


use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\AdminBundle\Form\ShopImageType;
use Buscolook\AdminBundle\Form\ShopType;
use Buscolook\WebBundle\Entity\GarmentRepository;
use Buscolook\WebBundle\Entity\Shop;
use Buscolook\WebBundle\Entity\ShopImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends BaseCrudController
{
    public function getViewPath()
    {
        return 'Shop';
    }

    public function getEntityClass()
    {
        return new Shop();
    }

    public function getEntityType()
    {
        return new ShopType($this->getRequest()->attributes->get('parentId'), $this->getEntityManager(), 'Bussiness');
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Shop');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }

    /**
     * @return GarmentRepository
     */
    private function getGarmentRepository()
    {
        return $this->getRepository('Garment');
    }

    public function addImageAction(Request $request, Shop $shop)
    {
        $image = new ShopImage();
        $imageForm = $this->createForm(new ShopImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $filename = $request->request->get('filename');
            $file = $request->files->get('files');
            $arrKey = $file[0]->getClientOriginalName();
            $size = $file[0]->getClientSize();
            $mimeType = $file[0]->getMimeType();
            $fileReturn = $uploadManager->uploadFile($request->files, $shop, 'tiendas/' . $shop->getBussiness()->getId() . '/shop', $filename[$arrKey]);
            $response = new JsonResponse();
            $array = ['files' => [['url' => $fileReturn['ObjectURL'],
                'thumbnail_url' => $fileReturn['ObjectURL'],
                'name' => $filename[$arrKey],
                'type' => $mimeType,
                'size' => $size,
                'delete_url' => $this->generateUrl('buscolook_admin_bussiness_delete_image', ['id' => $shop->getId(), 'filename' => $fileReturn['fileName']]),
                'delete_type' => 'POST']]];
            $response->setData($array);

            return $response;
        }
        return $this->render('BuscolookAdminBundle:Shop:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function deleteImageAction(Request $request, Shop $shop, $filename)
    {
        $image = new ShopImage();
        $imageForm = $this->createForm(new ShopImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('pventa/' . $shop->getId() . '/' . $filename);
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function subEditAction($parentId, $id)
    {
        $entity = $this->getEntityRepository()->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $garments = $this->getGarmentRepository()->findGarmentsByShop($id);

        $images = $this->getRepository('ShopImage')->findBy(['shop' => $id]);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'images' => $images,
                'parentId' => $parentId,
                'garments' => $garments
            ]
        );
    }

    public function subUpdateAction($parentId, $id, Request $request)
    {
        $entity = $this->getEntityRepository()->find($id);

        $em = $this->getEntityManager();

        $form = $this->createForm($this->getEntityType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('buscolook_admin_' . strtolower($this->getViewPath()) . '_edit', ['parentId'=>$parentId ,'id' => $id]));
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }
}