<?php
/**
 * Created by PhpStorm.
 * User: aramos
 * Date: 12/24/13
 * Time: 4:41 PM
 */

namespace Buscolook\AdminBundle\Controller;


use Buscolook\AdminBundle\Form\GarmentImageType;
use Buscolook\AdminBundle\Form\GarmentType;
use Buscolook\WebBundle\Entity\EntityImage;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\WebBundle\Entity\GarmentImage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GarmentController extends Controller
{
    public function createAction(Request $request, $parentId)
    {

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm($this->getEntityType(), $this->getEntityClass());
        $form->submit($request);

        if ($form->isValid()) {
            $form->getData()->addShop($form->getData()->getShop());
            $form->getData()->setActive(true);
            $em->persist($form->getData());
            $em->flush();

            return  $this->redirect($this->generateUrl('buscolook_admin_garment_edit',['parentId'=>$parentId,'id'=>$form->getData()->getId()]));
        } else {
            $return = $this->get('translator')->trans('forms.common.errors');
        }

        return $return;
    }

    public function addImageAction(Request $request, Garment $garment)
    {
        $image = new GarmentImage();
        $imageForm = $this->createForm(new GarmentImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $filename = $request->request->get('filename');
            $file = $request->files->get('files');
            $arrKey = $file[0]->getClientOriginalName();
            $size = $file[0]->getClientSize();
            $mimeType = $file[0]->getMimeType();
            $fileReturn = $uploadManager->uploadFile($file, $garment, 'prendas', $filename[$arrKey]);
            $response = new JsonResponse();
            $array = ['files' => [['url' => $fileReturn['ObjectURL'],
                'thumbnail_url' => $fileReturn['ObjectURL'],
                'name' => $filename[$arrKey],
                'type' => $mimeType,
                'size' => $size,
                'delete_url' => $this->generateUrl('buscolook_admin_bussiness_delete_image', ['id'=>$garment->getId(),'filename'=>$fileReturn['fileName']]),
                'delete_type' => 'POST']]];
            $response->setData($array);

            return $response;
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function deleteImageAction(Request $request, Garment $garment, $filename)
    {
        $image = new EntityImage();
        $imageForm = $this->createForm(new BussinessImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('prendas/'. $garment->getId() . '/' . $filename);
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function editTabImageAction(Request $request, $parentId, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BuscolookWebBundle:Garment')->find($id);
        $form = $this->createForm($this->getEntityType(), $entity);
        $images = $em->getRepository('BuscolookWebBundle:GarmentImage')->findBy(['garment' => $id]);

        return $this->render(
            sprintf('BuscolookAdminBundle:%s:form.html.twig', $this->getViewPath()),
            [
                'form' => $form->createView(),
                'entity' => $entity,
                'images' => $images,
                'parentId' => $parentId
            ]
        );
    }

    public function getViewPath()
    {
        return 'Garment';
    }

    public function getEntityClass()
    {
        return new Garment();
    }

    public function getEntityType()
    {
        return new GarmentType($this->getRequest()->attributes->get('parentId'), $this->getDoctrine()->getManager(), 'Shop');
    }

} 