<?php
namespace Buscolook\AdminBundle\Form;

use Buscolook\WebBundle\Form\EventListener\AddPasswordSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', ['attr' => ['id' => 'username','placeholder'=>'Nombre']])
            ->add('password', 'repeated', ['type' => 'password', 'first_options' => ['attr'=> ['placeholder'=>'Contraseña']],'second_options' => ['attr'=> ['placeholder'=>'Repite Contraseña']]])
            ->add('email', 'text',['attr' => ['id' => 'email','name'=>'email', 'placeholder'=>'E-mail']])
            ->add('locality', 'text',['attr' => ['id' => 'locality','name'=>'locality', 'placeholder'=>'Ciudad'],'mapped'=>false])
            ->add('register', 'submit');

        $builder->addEventSubscriber(new AddPasswordSubscriber());
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_user';
    }
}
