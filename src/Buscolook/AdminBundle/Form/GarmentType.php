<?php

namespace Buscolook\AdminBundle\Form;

use Buscolook\AdminBundle\Form\EventListener\ParentSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

class GarmentType extends AbstractType
{
    protected $shopId;
    protected $em;
    protected $entity;

    public function __construct($shopId, EntityManager $em, $entity)
    {
        $this->shopId = $shopId;
        $this->em = $em;
        $this->entity = $entity;
    }

     /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price','integer')
            ->add('priceDiscount')
            ->add('discount')
            ->add('stock')
            ->add('urlWeb')
            ->add('target')
            ->add('collection', 'entity',['class' => 'BuscolookWebBundle:Collection', 'property'=>'collection'])
            ->add('category', 'entity', ['class'=>'BuscolookWebBundle:Category',
                                         'property'=>'name',
                                         'query_builder' => function (EntityRepository $er) {
                                            return $er->createQueryBuilder('c')
                                                ->where('c.type = 2');
                                         }])
            ->add('colors')
            ->add('brand')
            ->add('highlight','entity',['class'=>'BuscolookWebBundle:Highlight','property'=>'area'])
            ->add('highlighted','checkbox' , ['required'=>false])
        ;
        $subscriber = new ParentSubscriber($this->shopId, $this->em, $this->entity);
        $builder->addEventSubscriber($subscriber);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Garment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_garment';
    }
}
