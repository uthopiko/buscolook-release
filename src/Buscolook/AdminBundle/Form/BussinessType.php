<?php

namespace Buscolook\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BussinessType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('cp')
            ->add('zona')
            ->add('Contact')
            ->add('phone')
            ->add('web')
            ->add('tiendaonline')
            ->add('email')
            ->add('logo')
            ->add('description')
            ->add('rs1')
            ->add('rs2')
            ->add('rs3')
            ->add('rs4')
            ->add('rs5')
            ->add('showcase')
            ->add('outlet')
            ->add('latitude')
            ->add('longitude')
            ->add('status')
            ->add('appmovil')
            ->add('createdAt')
            ->add('locality')
            ->add('province')
            ->add('bussinesstype')
            ->add('categories')
            ->add('user')
            ->add('target')
            ->add('highlight')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Bussiness'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_bussiness';
    }
}
