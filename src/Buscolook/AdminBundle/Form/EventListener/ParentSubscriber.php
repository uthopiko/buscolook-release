<?php
namespace Buscolook\AdminBundle\Form\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Tests\Helper\FormatterHelperTest;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\SecurityContext;

class ParentSubscriber implements EventSubscriberInterface
{

    protected $parentId;
    protected $em;
    protected $entity;

    public function __construct($parentId, EntityManager $em, $entity)
    {
        $this->parentId = $parentId;
        $this->entity = $entity;
        $this->em = $em;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SUBMIT => 'postSubmitData',
            FormEvents::PRE_SET_DATA => 'preSetData');

    }

    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        // check if the product object is "new"
        // If you didn't pass any data to the form, the data is "null".
        // This should be considered a new "Product"
        if (!$data || !$data->getId()) {
            $form->add('new', 'hidden', ['mapped' => false]);
        }
    }

    public function postSubmitData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();
        $exists = $form->has('new');
        if ($exists) {
            $entityObject = $this->em->getRepository('BuscolookWebBundle:' . $this->entity)->find($this->parentId);
            $setter = 'set' . $this->entity;
            $data->$setter($entityObject);
        }
    }
}