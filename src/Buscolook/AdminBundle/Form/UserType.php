<?php

namespace Buscolook\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('salt')
            ->add('password')
            ->add('email')
            ->add('isActive')
            ->add('name')
            ->add('lastname1')
            ->add('lastname_2')
            ->add('avatar')
            ->add('createdAt')
            ->add('address')
            ->add('cp')
            ->add('country')
            ->add('gender')
            ->add('movilPhone')
            ->add('urlfacebook')
            ->add('urltwitter')
            ->add('urlblog')
            ->add('observations')
            ->add('confirmed')
            ->add('idFacebook')
            ->add('locality')
            ->add('province')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_user';
    }
}
