<?php

namespace Buscolook\AdminBundle\Form;

use Buscolook\AdminBundle\Form\EventListener\ParentSubscriber;
use Buscolook\AdminBundle\Form\EventListener\ShopSubscriber;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShopType extends AbstractType
{
    protected $bussinessId;
    protected $em;
    protected $entity;

    public function __construct($bussinessId, EntityManager $em, $entity)
    {
        $this->bussinessId = $bussinessId;
        $this->em = $em;
        $this->entity = $entity;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('cp')
            ->add('zona')
            ->add('phone')
            ->add('schedule')
            ->add('email')
            ->add('description')
            ->add('googlemap')
            ->add('latitude')
            ->add('longitude')
            ->add('locality','entity',['class'=>'Buscolook\WebBundle\Entity\Locality','property'=>'locality'])
            ->add('province','entity',['class'=>'Buscolook\WebBundle\Entity\Province','property'=>'province'])
        ;

        $subscriber = new ParentSubscriber($this->bussinessId, $this->em, $this->entity);
        $builder->addEventSubscriber($subscriber);
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Shop'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_shop';
    }
}
