/* [ ---- Ebro Admin - extended form elements ---- ] */

	$(function() {
		//* datepicker
		ebro_datepicker.init();
	});
	

	//* datepicker
	ebro_datepicker = {
		init: function() {
			if($('.ebro_datepicker').length) {
				$('.ebro_datepicker').datepicker()
			}
			if( ($('#dpStart').length) && ($('#dpEnd').length) ) {
				$('#dpStart').datepicker().on('changeDate', function(e){
					$('#dpEnd').datepicker('setStartDate', e.date);
				});
				$('#dpEnd').datepicker().on('changeDate', function(e){
					$('#dpStart').datepicker('setEndDate', e.date)
				});
			}
		}
	};