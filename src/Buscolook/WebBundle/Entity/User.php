<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\ExecutionContext;
use Doctrine\Common\Collections\ArrayCollection;
use Buscolook\WebBundle\Manager\UserManager;

/**
 * Buscolook\WebBundle\Entity\User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User extends UserManager implements UserInterface, \Serializable
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="username",type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(name="salt",type="string", length=32)
     */
    private $salt;

    /**
     * @ORM\Column(name="password",type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(name="email",type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToMany(targetEntity="Buscolook\WebBundle\Entity\Role")
     * @ORM\JoinTable(name="user_role",
     *   joinColumns={
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")
     *   }
     * )
     */
    private $user_roles;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_1", type="string", length=255, nullable=true)
     */
    private $lastname1;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_2", type="string", length=255, nullable=true)
     */
    private $lastname_2;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="user")
     * @ORM\JoinColumn(name="locality_id", referencedColumnName="id", nullable=true)
     */
    private $locality;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=255, nullable=true)
     */
    private $cp;

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="user")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=true)
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="movil_phone", type="string", length=255, nullable=true)
     */
    private $movilPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="url_facebook", type="string", length=255, nullable=true)
     */
    private $urlfacebook;

    /**
     * @var string
     *
     * @ORM\Column(name="url_twitter", type="string", length=255, nullable=true)
     */
    private $urltwitter;

    /**
     * @var string
     *
     * @ORM\Column(name="url_blog", type="string", length=255, nullable=true)
     */
    private $urlblog;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="text", nullable=true)
     */
    private $observations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="confirmed", type="boolean", nullable=true)
     */
    private $confirmed;

    /**
     * @var string
     *
     * @ORM\Column(name="id_facebook", type="string", length=255, nullable=true)
     */
    private $idFacebook;

    /**
     * @ORM\OneToMany(targetEntity="Bussiness", mappedBy="user")
     */
    private $bussiness;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity="Fichaje", mappedBy="buscolooker")
     */
    private $fichajes;


    public $file;

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->isActive = true;
        $this->confirmed = true;
        $this->user_roles = new ArrayCollection();
        $this->fichajes = new ArrayCollection();
    }


	/**
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return $this->user_roles->toArray();
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Add user_roles
     *
     * @param Role $userRoles
     */
    public function addRole(Role $userRoles)
    {
        $this->user_roles[] = $userRoles;
    }

    /**
     * Get user_roles
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUserRoles()
    {
        return $this->user_roles;
    }

    public function setUserRoles($user_roles)
    {
        $this->user_roles = $user_roles;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set lastname1
     *
     * @param string $lastname1
     * @return User
     */
    public function setLastname1($lastname1)
    {
        $this->lastname1 = $lastname1;
    
        return $this;
    }

    /**
     * Get lastname1
     *
     * @return string 
     */
    public function getLastname1()
    {
        return $this->lastname1;
    }

    /**
     * Set lastname_2
     *
     * @param string $lastname2
     * @return User
     */
    public function setLastname2($lastname2)
    {
        $this->lastname_2 = $lastname2;
    
        return $this;
    }

    /**
     * Get lastname_2
     *
     * @return string 
     */
    public function getLastname2()
    {
        return $this->lastname_2;
    }

    /**
     * @return string
     */
    public function getFullUserName() {
        return sprintf(
            '%s %s %s',
            $this->name,
            $this->lastname1,
            $this->lastname_2
        );
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    
        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return User
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    
        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    
        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set movilPhone
     *
     * @param string $movilPhone
     * @return User
     */
    public function setMovilPhone($movilPhone)
    {
        $this->movilPhone = $movilPhone;
    
        return $this;
    }

    /**
     * Get movilPhone
     *
     * @return string 
     */
    public function getMovilPhone()
    {
        return $this->movilPhone;
    }

    /**
     * Set urlfacebook
     *
     * @param string $urlfacebook
     * @return User
     */
    public function setUrlfacebook($urlfacebook)
    {
        $this->urlfacebook = $urlfacebook;
    
        return $this;
    }

    /**
     * Get urlfacebook
     *
     * @return string 
     */
    public function getUrlfacebook()
    {
        return $this->urlfacebook;
    }

    /**
     * Set urltwitter
     *
     * @param string $urltwitter
     * @return User
     */
    public function setUrltwitter($urltwitter)
    {
        $this->urltwitter = $urltwitter;
    
        return $this;
    }

    /**
     * Get urltwitter
     *
     * @return string 
     */
    public function getUrltwitter()
    {
        return $this->urltwitter;
    }

    /**
     * Set urlblog
     *
     * @param string $urlblog
     * @return User
     */
    public function setUrlblog($urlblog)
    {
        $this->urlblog = $urlblog;
    
        return $this;
    }

    /**
     * Get urlblog
     *
     * @return string 
     */
    public function getUrlblog()
    {
        return $this->urlblog;
    }

    /**
     * Set observations
     *
     * @param string $observations
     * @return User
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;
    
        return $this;
    }

    /**
     * Get observations
     *
     * @return string 
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     * @return User
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    
        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * Set idFacebook
     *
     * @param string $idFacebook
     * @return User
     */
    public function setIdFacebook($idFacebook)
    {
        $this->idFacebook = $idFacebook;
    
        return $this;
    }

    /**
     * Get idFacebook
     *
     * @return string 
     */
    public function getIdFacebook()
    {
        return $this->idFacebook;
    }

    /**
     * Set locality
     *
     * @param \Buscolook\WebBundle\Entity\Locality $locality
     * @return User
     */
    public function setLocality(\Buscolook\WebBundle\Entity\Locality $locality = null)
    {
        $this->locality = $locality;
    
        return $this;
    }

    /**
     * Get locality
     *
     * @return \Buscolook\WebBundle\Entity\Locality 
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set province
     *
     * @param \Buscolook\WebBundle\Entity\Province $province
     * @return User
     */
    public function setProvince(\Buscolook\WebBundle\Entity\Province $province = null)
    {
        $this->province = $province;
    
        return $this;
    }

    /**
     * Get province
     *
     * @return \Buscolook\WebBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    public function getFullname(){
            return $this->name . ' ' . $this->lastname1 . ' ' . $this->lastname_2 . ' ' . $this->username;

        return $this->username;
    }

    /**
     * Serializes the content of the current User object
     * @return string
     */
    public function serialize()
    {
        return \json_encode(
                array($this->username, $this->password, $this->salt,
                        $this->user_roles, $this->id));
    }

    /**
     * Unserializes the given string in the current User object
     * @param serialized
     */
    public function unserialize($serialized)
    {
        list($this->username, $this->password, $this->salt,
                        $this->user_roles, $this->id) = \json_decode(
                $serialized);
    }

    /**
     * @return mixed
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Bussiness
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        $this->file = $logo;
        // check if we have an old image path
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getLogo()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->logo = $filename.'.'.$this->getLogo()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Get logo
     *
     * @return string
     */
    public function getLogoWebPath()
    {
        return '/uploads/documents/buscolooker/'.$this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->logo
            ? null
            : $this->getUploadRootDir().'/'.$this->logo;
    }

    public function getWebPath()
    {
        return null === $this->logo
            ? null
            : $this->getUploadDir().'/'.$this->logo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    /**
     * @param mixed $fichajes
     */
    public function setFichajes($fichajes)
    {
        $this->fichajes = $fichajes;
    }

    /**
     * @return mixed
     */
    public function getFichajes()
    {
        return $this->fichajes;
    }

}