<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class CategoryRepository extends EntityRepository
{
    public function findFilters()
    {
        $q = $this->createQueryBuilder('c')
            ->select('c.id,c.name,c.slug');

        return $q->getQuery()->getResult();
    }
}