<?php
namespace Buscolook\WebBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fichaje
 *
 * @ORM\Table(name="fichaje")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\FichajeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Fichaje {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name",type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="fichaje")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=true)
     */
    private $bussiness;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fichaje")
     * @ORM\JoinColumn(name="buscolooker_id", referencedColumnName="id", nullable=true)
     */
    private $buscolooker;

    /**
     * @var string
     *
     * @ORM\Column(type="boolean",name="published", nullable=false)
     */
    private $published = false;

    /**
     * @var string
     *
     * @ORM\Column(name="deleted_at",type="datetime", nullable=true)
     */
    private $deletedAt = null;

    /**
     * @ORM\Column(name="created_at",type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="FichajeImage", mappedBy="fichaje")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="Fav", mappedBy="fichaje")
     */
    private $favs;

    /**
     * @ORM\OneToMany(targetEntity="Garment", mappedBy="fichaje")
     */
    private $garments;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="fichaje", cascade={"persist"})
     */
    private $comments;

    public function __construct() {
        $this->garments = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @param mixed $bussiness
     */
    public function setBussiness($bussiness)
    {
        $this->bussiness = $bussiness;
    }

    /**
     * @return mixed
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return string
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param string $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @return string
     */
    public function getFavs()
    {
        return $this->favs;
    }

    public function isFav($fichajeId, $userId)
    {
        $return = false;

        $favs = $this->favs;
        foreach ($favs as $fav)
        {
            if ($fav->getFichaje()->getId() == $fichajeId && $fav->getUser()->getId() == $userId) {
                return true;
            }
        }

        return $return;

    }

    /**
     * @return int
     */
    public function getNumberOfFavorites() {
        return $this->favs->count();
    }

    /**
     * @param mixed $buscolooker
     */
    public function setBuscolooker($buscolooker)
    {
        $this->buscolooker = $buscolooker;
    }

    /**
     * @return mixed
     */
    public function getBuscolooker()
    {
        return $this->buscolooker;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return Garment[]
     */
    public function getGarments() {
        return $this->garments;
    }

    /**
     * @param array $garments
     * @return $this
     */
    public function setGarments(array $garments) {
        $this->garments = $garments;

        return $this;
    }

    /**
     * @param Garment $garment
     * @return $this
     */
    public function addGarment(Garment $garment) {
        if (!$this->garments->contains($garment)) {
            $this->garments->add($garment);
            $garment->setFichaje($this);
        }

        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getComments() {
        return $this->comments;
    }

    /**
     * @param array $comments
     * @return $this
     */
    public function setComments(array $comments) {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @param Comment $comment
     * @return $this
     */
    public function addComment(Comment $comment) {
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment);
            $comment->setFichaje($this);
        }

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }
} 