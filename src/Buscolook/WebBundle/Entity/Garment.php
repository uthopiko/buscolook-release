<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Garment
 *
 * @ORM\Table(name="garment")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\GarmentRepository")
 */
class Garment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Target", inversedBy="garment")
     * @ORM\JoinColumn(name="target_id", referencedColumnName="id", nullable=true)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @ORM\ManyToMany(targetEntity="Shop")
     * @ORM\JoinTable(name="garment_shop",
     *   joinColumns={@ORM\JoinColumn(name="garment_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="shop_id", referencedColumnName="id")}
     * )
     */
    private $shops;

    /**
     * @ORM\ManyToOne(targetEntity="Collection", inversedBy="garment")
     * @ORM\JoinColumn(name="collection_id", referencedColumnName="id", nullable=true)
     */
    private $collection;

    /**
     * @ORM\ManyToOne(targetEntity="SubCategory", inversedBy="garment")
     * @ORM\JoinColumn(name="subcategory_id", referencedColumnName="id", nullable=true)
     */
    private $subcategory;

    /**
     * @ORM\ManyToMany(targetEntity="Color")
     * @ORM\JoinTable(name="garment_color",
     *   joinColumns={@ORM\JoinColumn(name="garment_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="color_id", referencedColumnName="id")}
     * )
     */
    private $colors;

    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="garment")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=true)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255, nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="price_discount", type="string", length=255, nullable=true)
     */
    private $priceDiscount;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="string", length=255, nullable=true)
     */
    private $discount;

    /**
     * @var string
     *
     * @ORM\Column(name="hearts", type="string", length=255, nullable=true)
     */
    private $hearts;

    /**
     * @var string
     *
     * @ORM\Column(name="stock", type="string", length=255, nullable=true)
     */
    private $stock;

    /**
     * @ORM\ManyToOne(targetEntity="Highlight", inversedBy="garment")
     * @ORM\JoinColumn(name="highlight_id", referencedColumnName="id", nullable=true)
     */
    private $highlight;

    /**
     * @ORM\Column(name="highlighted",type="boolean")
     */
    private $highlighted = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="UrlWeb", type="string", length=255, nullable=true)
     */
    private $urlWeb;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="GarmentImage", mappedBy="garment")
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="Fav", mappedBy="garment")
     */
    private $favs;

    /**
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(name="active",type="boolean")
     */
    private $active = true;

    /**
     * @ORM\ManyToOne(targetEntity="Fichaje")
     * @ORM\JoinColumn(name="fichaje_id", referencedColumnName="id", nullable=true)
     */
    private $fichaje;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->colors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Garment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Garment
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceDiscount
     *
     * @param string $priceDiscount
     * @return Garment
     */
    public function setPriceDiscount($priceDiscount)
    {
        $this->priceDiscount = $priceDiscount;

        return $this;
    }

    /**
     * Get priceDiscount
     *
     * @return string
     */
    public function getPriceDiscount()
    {
        return $this->priceDiscount;
    }

    /**
     * Set discount
     *
     * @param string $discount
     * @return Garment
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set shop
     *
     * @param \Buscolook\WebBundle\Entity\Shop $shop
     * @return Garment
     */
    public function setShop(\Buscolook\WebBundle\Entity\Shop $shop)
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * Get shop
     *
     * @return \Buscolook\WebBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set hearts
     *
     * @param string $hearts
     * @return Garment
     */
    public function setHearts($hearts)
    {
        $this->hearts = $hearts;

        return $this;
    }

    /**
     * Get hearts
     *
     * @return string
     */
    public function getHearts()
    {
        return $this->hearts;
    }

    /**
     * Set stock
     *
     * @param string $stock
     * @return Garment
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Garment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set urlWeb
     *
     * @param string $urlWeb
     * @return Garment
     */
    public function setUrlWeb($urlWeb)
    {
        $this->urlWeb = $urlWeb;

        return $this;
    }

    /**
     * Get urlWeb
     *
     * @return string
     */
    public function getUrlWeb()
    {
        return $this->urlWeb;
    }

    /**
     * Set target
     *
     * @param \Buscolook\WebBundle\Entity\Target $target
     * @return Garment
     */
    public function setTarget(\Buscolook\WebBundle\Entity\Target $target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \Buscolook\WebBundle\Entity\Target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Add shops
     *
     * @param \Buscolook\WebBundle\Entity\Shop $shops
     * @return Garment
     */
    public function addShop(\Buscolook\WebBundle\Entity\Shop $shops)
    {
        $this->shops[] = $shops;

        return $this;
    }

    /**
     * Remove shops
     *
     * @param \Buscolook\WebBundle\Entity\Shop $shops
     */
    public function removeShop(\Buscolook\WebBundle\Entity\Shop $shops)
    {
        $this->shops->removeElement($shops);
    }

    /**
     * Get shops
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * Set collection
     *
     * @param \Buscolook\WebBundle\Entity\Collection $collection
     * @return Garment
     */
    public function setCollection(\Buscolook\WebBundle\Entity\Collection $collection = null)
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * Get collection
     *
     * @return \Buscolook\WebBundle\Entity\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Set subcategory
     *
     * @param \Buscolook\WebBundle\Entity\SubCategory $subcategory
     * @return Garment
     */
    public function setSubcategory(\Buscolook\WebBundle\Entity\SubCategory $subcategory = null)
    {
        $this->subcategory = $subcategory;

        return $this;
    }

    /**
     * Get subcategory
     *
     * @return \Buscolook\WebBundle\Entity\SubCategory
     */
    public function getSubcategory()
    {
        return $this->subcategory;
    }

    /**
     * Add colors
     *
     * @param \Buscolook\WebBundle\Entity\Color $colors
     * @return Garment
     */
    public function addColor(\Buscolook\WebBundle\Entity\Color $colors)
    {
        $this->colors[] = $colors;

        return $this;
    }

    /**
     * Remove colors
     *
     * @param \Buscolook\WebBundle\Entity\Color $colors
     */
    public function removeColor(\Buscolook\WebBundle\Entity\Color $colors)
    {
        $this->colors->removeElement($colors);
    }

    /**
     * Get colors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * Set brand
     *
     * @param \Buscolook\WebBundle\Entity\Brand $brand
     * @return Garment
     */
    public function setBrand(\Buscolook\WebBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \Buscolook\WebBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set highlight
     *
     * @param \Buscolook\WebBundle\Entity\Highlight $highlight
     * @return Garment
     */
    public function setHighlight(\Buscolook\WebBundle\Entity\Highlight $highlight = null)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Get highlight
     *
     * @return \Buscolook\WebBundle\Entity\Highlight
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * @param mixed $highlighted
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;
    }

    /**
     * @return mixed
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }


    /**
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getFavs()
    {
        return $this->favs;
    }

    public function isFav($garmentId, $userId)
    {
        $return = false;

        $favs = $this->favs;
        foreach ($favs as $fav)
        {
            if ($fav->getGarment()->getId() == $garmentId && $fav->getUser()->getId() == $userId) {
                return true;
            }
        }

        return $return;

    }

    /**
     * @return int
     */
    public function getNumberOfFavorites() {
        return $this->favs->count();
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $fichaje
     */
    public function setFichaje($fichaje)
    {
        $this->fichaje = $fichaje;
    }

    /**
     * @return mixed
     */
    public function getFichaje()
    {
        return $this->fichaje;
    }
}