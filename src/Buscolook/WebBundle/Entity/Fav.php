<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fav
 *
 * @ORM\Table(name="fav")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\FavRepository")
 */
class Fav
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Look", inversedBy="fav")
     * @ORM\JoinColumn(name="look_id", referencedColumnName="id", nullable=true)
     */
    private $look;

    /**
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="fav")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id", nullable=true)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="fav")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=true)
     */
    private $bussiness;

    /**
     * @ORM\ManyToOne(targetEntity="Garment", inversedBy="fav")
     * @ORM\JoinColumn(name="garment_id", referencedColumnName="id", nullable=true)
     */
    private $garment;

    /**
     * @ORM\ManyToOne(targetEntity="Fichaje", inversedBy="fav")
     * @ORM\JoinColumn(name="fichaje_id", referencedColumnName="id", nullable=true)
     */
    private $fichaje;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fav")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Fav
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set look
     *
     * @param \Buscolook\WebBundle\Entity\Look $look
     * @return Fav
     */
    public function setLook(\Buscolook\WebBundle\Entity\Look $look = null)
    {
        $this->look = $look;
    
        return $this;
    }

    /**
     * Get look
     *
     * @return \Buscolook\WebBundle\Entity\Look 
     */
    public function getLook()
    {
        return $this->look;
    }

    /**
     * Set shop
     *
     * @param \Buscolook\WebBundle\Entity\Shop $shop
     * @return Fav
     */
    public function setShop(\Buscolook\WebBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;
    
        return $this;
    }

    /**
     * Get shop
     *
     * @return \Buscolook\WebBundle\Entity\Shop 
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set bussiness
     *
     * @param \Buscolook\WebBundle\Entity\Bussiness $bussiness
     * @return Fav
     */
    public function setBussiness(\Buscolook\WebBundle\Entity\Bussiness $bussiness = null)
    {
        $this->bussiness = $bussiness;

        return $this;
    }

    /**
     * Get bussiness
     *
     * @return \Buscolook\WebBundle\Entity\Bussiness
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }


    /**
     * Set garment
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garment
     * @return Fav
     */
    public function setGarment(\Buscolook\WebBundle\Entity\Garment $garment = null)
    {
        $this->garment = $garment;
    
        return $this;
    }

    /**
     * Get garment
     *
     * @return \Buscolook\WebBundle\Entity\Garment 
     */
    public function getGarment()
    {
        return $this->garment;
    }

    /**
     * Set fichaje
     *
     * @param \Buscolook\WebBundle\Entity\Fichaje $fichaje
     * @return Fav
     */
    public function setFichaje(\Buscolook\WebBundle\Entity\Fichaje $fichaje = null)
    {
        $this->fichaje = $fichaje;

        return $this;
    }

    /**
     * Get fichaje
     *
     * @return \Buscolook\WebBundle\Entity\Fichaje
     */
    public function getFichaje()
    {
        return $this->fichaje;
    }

    /**
     * Set user
     *
     * @param \Buscolook\WebBundle\Entity\User $user
     * @return Fav
     */
    public function setUser(\Buscolook\WebBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Buscolook\WebBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}