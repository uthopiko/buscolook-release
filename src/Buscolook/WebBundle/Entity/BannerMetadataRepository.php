<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class BannerMetadataRepository extends EntityRepository
{
    public function findBannerImages($bannerPosition)
    {
        $q = $this->createQueryBuilder('bm')
            ->select('bm.image, bm.title, bm.link')
            ->innerJoin('bm.banner','b')
            ->where('b.position = :position')
            ->setParameter('position',$bannerPosition);

        return $q->getQuery()->getResult();
    }
}