<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Otrascategoriastienda
 *
 * @ORM\Table(name="OtrasCategoriasTienda")
 * 
 */
class Otrascategoriastienda
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Categoria", type="string", length=255, nullable=true)
     */
    private $categoria;


}
