<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class GarmentRepository extends EntityRepository
{
    public function findGarmentsByShop($shopId)
    {
        $q = $this->createQueryBuilder('g')
            ->join('g.shops', 's')
            ->where('s.id = :shopId')
            ->setParameter('shopId', $shopId);

        return $q->getQuery()->getResult();
    }

    public function findGarmentsByBussiness($bussinessId)
    {
        $q = $this->createQueryBuilder('g')
            ->join('g.shops', 's')
            ->join('s.bussiness','b')
            ->where('b.id = :bussinessId')
            ->setParameter('bussinessId', $bussinessId);

        return $q->getQuery()->getResult();
    }

    public function findHomeGarments()
    {
        $q = $this->createQueryBuilder('g')
            ->where('g.highlighted = 1')
            ->setMaxResults(3);

        return $q->getQuery()->getResult();
    }

    public function findGarmentHighlighted($category, $zona)
    {
        $q = $this->createQueryBuilder('g');
        if ($category != Category::ALL){
            $q->join('g.category', 'c')
              ->andWhere('c.slug = :category')
              ->setParameter('category',$category);
        }

        if ($zona != Locality::ALL) {
            $q->join('g.shops', 'sh')
              ->join('sh.locality','l')
              ->andWhere('l.slug = :zona')
              ->setParameter('zona',$zona);
        }
            $q->andWhere('g.highlighted = 1 AND g.active = 1');

        return $q->getQuery()->getResult();
    }

    public function findPublishedFichajes($category, $zona, $search)
    {
        $q = $this->createQueryBuilder('g');
        $q->select('g,f');
        $q->join('g.fichaje','f');
        $q->join('g.shops', 'sh');
        if ($category != Category::ALL){
            $q->join('g.subcategory', 'c')
                ->andWhere('c.slug = :category')
                ->setParameter('category',$category);
        }

        if ($zona != Locality::ALL) {
            $q->join('sh.locality','l')
                ->andWhere('l.slug = :zona')
                ->setParameter('zona',$zona);
        }

        if ($search != null) {
            $q->join('sh.bussiness','b');
            $q->andWhere('f.name LIKE :search OR sh.name LIKE :search OR b.name LIKE :search');
            $q->setParameter('search','%'.$search.'%');
        }

        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        $q->groupBy('g.fichaje');

        return $q->getQuery()->getResult();
    }
}