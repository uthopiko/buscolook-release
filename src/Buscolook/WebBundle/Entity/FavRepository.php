<?php

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class FavRepository extends EntityRepository
{
    public function findFavouritesByUser($userId)
    {
        $q = $this->createQueryBuilder('f')
            ->where('f.user = :userId')
            ->andWhere('f.fichaje IS NOT null')
            ->setParameter('userId',$userId);

        return $q->getQuery()->getResult();
    }

    public function findFollowingByUser($userId)
    {
        $q = $this->createQueryBuilder('f')
            ->where('f.user = :userId')
            ->andWhere('f.bussiness IS NOT null')
            ->setParameter('userId',$userId);

        return $q->getQuery()->getResult();
    }
}