<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Look
 *
 * @ORM\Table(name="Look")
 * @ORM\Entity
 */
class Look
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Garment")
     * @ORM\JoinTable(name="look_garment",
     *   joinColumns={@ORM\JoinColumn(name="look_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="garment_id", referencedColumnName="id")}
     * )
     */
    private $garments;
    
    /**
     * @ORM\ManyToMany(targetEntity="Color")
     * @ORM\JoinTable(name="look_color",
     *   joinColumns={@ORM\JoinColumn(name="look_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="color_id", referencedColumnName="id")}
     * )
     */
    private $colors;
    
    /**
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(name="look_category",
     *   joinColumns={@ORM\JoinColumn(name="look_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comment")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="image_name", type="string", length=255, nullable=true)
     */
    private $image_name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="hearts", type="smallint", nullable=true)
     */
    private $hearts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="highlighted", type="boolean", nullable=true)
     */
    private $highlighted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="published", type="boolean", nullable=false)
     */
    private $published;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->garments = new \Doctrine\Common\Collections\ArrayCollection();
        $this->colors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Look
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image_name
     *
     * @param string $imageName
     * @return Look
     */
    public function setImageName($imageName)
    {
        $this->image_name = $imageName;
    
        return $this;
    }

    /**
     * Get image_name
     *
     * @return string 
     */
    public function getImageName()
    {
        return $this->image_name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Look
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Look
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Look
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set hearts
     *
     * @param integer $hearts
     * @return Look
     */
    public function setHearts($hearts)
    {
        $this->hearts = $hearts;
    
        return $this;
    }

    /**
     * Get hearts
     *
     * @return integer 
     */
    public function getHearts()
    {
        return $this->hearts;
    }

    /**
     * Set highlighted
     *
     * @param boolean $highlighted
     * @return Look
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;
    
        return $this;
    }

    /**
     * Get highlighted
     *
     * @return boolean 
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return Look
     */
    public function setPublished($published)
    {
        $this->published = $published;
    
        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set user
     *
     * @param \Buscolook\WebBundle\Entity\User $user
     * @return Look
     */
    public function setUser(\Buscolook\WebBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Buscolook\WebBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add garments
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garments
     * @return Look
     */
    public function addGarment(\Buscolook\WebBundle\Entity\Garment $garments)
    {
        $this->garments[] = $garments;
    
        return $this;
    }

    /**
     * Remove garments
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garments
     */
    public function removeGarment(\Buscolook\WebBundle\Entity\Garment $garments)
    {
        $this->garments->removeElement($garments);
    }

    /**
     * Get garments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGarments()
    {
        return $this->garments;
    }

    /**
     * Add colors
     *
     * @param \Buscolook\WebBundle\Entity\Color $colors
     * @return Look
     */
    public function addColor(\Buscolook\WebBundle\Entity\Color $colors)
    {
        $this->colors[] = $colors;
    
        return $this;
    }

    /**
     * Remove colors
     *
     * @param \Buscolook\WebBundle\Entity\Color $colors
     */
    public function removeColor(\Buscolook\WebBundle\Entity\Color $colors)
    {
        $this->colors->removeElement($colors);
    }

    /**
     * Get colors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * Add categories
     *
     * @param \Buscolook\WebBundle\Entity\Category $categories
     * @return Look
     */
    public function addCategorie(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;
    
        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Buscolook\WebBundle\Entity\Category $categories
     */
    public function removeCategorie(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set createdBy
     *
     * @param \Buscolook\WebBundle\Entity\User $createdBy
     * @return Look
     */
    public function setCreatedBy(\Buscolook\WebBundle\Entity\User $createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Buscolook\WebBundle\Entity\User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}