<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PressImage
 *
 * @ORM\Table(name="press_image")
 * @ORM\Entity
 */
class PressImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Press", inversedBy="press_image")
     * @ORM\JoinColumn(name="press_id", referencedColumnName="id", nullable=false)
     */
    private $press;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return PressImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return PressImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    
        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set press
     *
     * @param \Buscolook\WebBundle\Entity\Press $press
     * @return PressImage
     */
    public function setPress(\Buscolook\WebBundle\Entity\Press $press)
    {
        $this->press = $press;
    
        return $this;
    }

    /**
     * Get press
     *
     * @return \Buscolook\WebBundle\Entity\Press 
     */
    public function getPress()
    {
        return $this->press;
    }
}