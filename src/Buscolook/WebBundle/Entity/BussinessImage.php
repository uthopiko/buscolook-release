<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BussinessImage
 *
 * @ORM\Table(name="bussiness_image")
 * @ORM\Entity
 */
class BussinessImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="bussiness_image")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=false)
     */
    private $bussiness;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return BussinessImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return BussinessImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    
        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set bussiness
     *
     * @param \Buscolook\WebBundle\Entity\Bussiness $bussiness
     * @return BussinessImage
     */
    public function setBussiness(\Buscolook\WebBundle\Entity\Bussiness $bussiness)
    {
        $this->bussiness = $bussiness;
    
        return $this;
    }

    /**
     * Get bussiness
     *
     * @return \Buscolook\WebBundle\Entity\Bussiness 
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}