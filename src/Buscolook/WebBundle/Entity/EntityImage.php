<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PressImage
 *
 * @ORM\Table(name="entity_image")
 * @ORM\Entity
 */
class EntityImage
{
    const ENTITY_TYPE_BUSSINESS = 1;
    const ENTITY_TYPE_SHOP = 2;
    const ENTITY_TYPE_GARMENT = 3;
    const ENTITY_TYPE_PRESS = 4;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_id", type="integer", length=255, nullable=false)
     */
    private $entityId;

    /**
     * @var string
     *
     * @ORM\Column(name="entity_type", type="string", length=255, nullable=false)
     */
    private $entityType;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;

    /**
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(name="image_category",
     *   joinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set image
     *
     * @param string $image
     * @return PressImage
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return PressImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean
     */
    public function getPrincipal()
    {
        return $this->principal;
    }


    /**
     * Add categories
     *
     * @param \Buscolook\WebBundle\Entity\Category $category
     * @return Category
     */
    public function addCategory(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove colors
     *
     * @param \Buscolook\WebBundle\Entity\Category $categories
     */
    public function removeCategory(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->$categories->removeElement($categories);
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Get colors
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    public function setEntityId($entity_id)
    {
        $this->entityId = $entity_id;
    }

    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param string $entityType
     */
    public function setEntityType($entityType)
    {
        $this->entityType = $entityType;
    }

    /**
     * @return string
     */
    public function getEntityType()
    {
        return $this->entityType;
    }



}