<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GarmentImage
 *
 * @ORM\Table(name="inspiration_image")
 * @ORM\Entity
 */
class InspirationImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Inspiration", inversedBy="inspiration_image")
     * @ORM\JoinColumn(name="inspiration_id", referencedColumnName="id", nullable=false)
     */
    private $inspiration;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return GarmentImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return GarmentImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    
        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set inspiration
     *
     * @param \Buscolook\WebBundle\Entity\Inspiration $inspiration
     * @return InspirationImage
     */
    public function setInspiration(\Buscolook\WebBundle\Entity\Inspiration $inspiration)
    {
        $this->inspiration = $inspiration;
    
        return $this;
    }

    /**
     * Get inspiration
     *
     * @return \Buscolook\WebBundle\Entity\Inspiration 
     */
    public function getInspiration()
    {
        return $this->inspiration;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


}