<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Bussiness
 *
 * @ORM\Table(name="bussiness")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\BussinessRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Bussiness
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="bussiness")
     * @ORM\JoinColumn(name="locality_id", referencedColumnName="id", nullable=true)
     */
    private $locality;

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="bussiness")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=true)
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="CP", type="string", length=10, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="Zona", type="string", length=255, nullable=true)
     */
    private $zona;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=255, nullable=true)
     */
    private $contact;

    /**
     * @ORM\ManyToOne(targetEntity="BussinessType", inversedBy="bussiness")
     * @ORM\JoinColumn(name="bussinesstype_id", referencedColumnName="id", nullable=false)
     */
    private $bussinesstype;

    /**
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(name="bussiness_category",
     *   joinColumns={@ORM\JoinColumn(name="bussiness_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")}
     * )
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bussiness",cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     */
    private $web;

    /**
     * @var string
     *
     * @ORM\Column(name="tienda_online", type="string", length=255, nullable=true)
     */
    private $tiendaonline;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="Target", inversedBy="bussiness")
     * @ORM\JoinColumn(name="target_id", referencedColumnName="id", nullable=true)
     */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=255, nullable=true)
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="RS1", type="string", length=255, nullable=true)
     */
    private $rs1;

    /**
     * @var string
     *
     * @ORM\Column(name="RS2", type="string", length=255, nullable=true)
     */
    private $rs2;

    /**
     * @var string
     *
     * @ORM\Column(name="RS3", type="string", length=255, nullable=true)
     */
    private $rs3;

    /**
     * @var string
     *
     * @ORM\Column(name="RS4", type="string", length=255, nullable=true)
     */
    private $rs4;

    /**
     * @var string
     *
     * @ORM\Column(name="RS5", type="string", length=255, nullable=true)
     */
    private $rs5;

    /**
     * @var string
     *
     * @ORM\Column(name="showcase", type="string", length=255, nullable=true)
     */
    private $showcase;

    /**
     * @ORM\ManyToOne(targetEntity="Highlight", inversedBy="bussiness")
     * @ORM\JoinColumn(name="highlight_id", referencedColumnName="id", nullable=true)
     */
    private $highlight;

    /**
     * @var boolean
     *
     * @ORM\Column(name="outlet", type="boolean", nullable=true)
     */
    private $outlet;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="appmovil", type="boolean", nullable=true)
     */
    private $appmovil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="Fav", mappedBy="bussiness")
     */
    private $favs;

    /**
     * @ORM\OneToMany(targetEntity="Shop", mappedBy="bussiness")
     */
    private $shops;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getAbsolutePath()
    {
        return null === $this->logo
            ? null
            : $this->getUploadRootDir().'/'.$this->logo;
    }

    public function getWebPath()
    {
        return null === $this->logo
            ? null
            : $this->getUploadDir().'/'.$this->logo;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Bussiness
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Bussiness
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Bussiness
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set zona
     *
     * @param string $zona
     * @return Bussiness
     */
    public function setZona($zona)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set Contact
     *
     * @param string $contact
     * @return Bussiness
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get Contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Bussiness
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set web
     *
     * @param string $web
     * @return Bussiness
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set tiendaonline
     *
     * @param string $tiendaonline
     * @return Bussiness
     */
    public function setTiendaonline($tiendaonline)
    {
        $this->tiendaonline = $tiendaonline;

        return $this;
    }

    /**
     * Get tiendaonline
     *
     * @return string
     */
    public function getTiendaonline()
    {
        return $this->tiendaonline;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Bussiness
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    public $file;

    /**
     * Set logo
     *
     * @param string $logo
     * @return Bussiness
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        $this->file = $logo;
        // check if we have an old image path
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getLogo()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->logo = $filename.'.'.$this->getLogo()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->file->move($this->getUploadRootDir(), $this->logo);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }


    /**
     * Get logo
     *
     * @return string
     */
    public function getLogoWebPath()
    {
        return '/uploads/documents/'.$this->logo;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bussiness
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set rs1
     *
     * @param string $rs1
     * @return Bussiness
     */
    public function setRs1($rs1)
    {
        $this->rs1 = $rs1;

        return $this;
    }

    /**
     * Get rs1
     *
     * @return string
     */
    public function getRs1()
    {
        return $this->rs1;
    }

    /**
     * Set rs2
     *
     * @param string $rs2
     * @return Bussiness
     */
    public function setRs2($rs2)
    {
        $this->rs2 = $rs2;

        return $this;
    }

    /**
     * Get rs2
     *
     * @return string
     */
    public function getRs2()
    {
        return $this->rs2;
    }

    /**
     * Set rs3
     *
     * @param string $rs3
     * @return Bussiness
     */
    public function setRs3($rs3)
    {
        $this->rs3 = $rs3;

        return $this;
    }

    /**
     * Get rs3
     *
     * @return string
     */
    public function getRs3()
    {
        return $this->rs3;
    }

    /**
     * Set rs4
     *
     * @param string $rs4
     * @return Bussiness
     */
    public function setRs4($rs4)
    {
        $this->rs4 = $rs4;

        return $this;
    }

    /**
     * Get rs4
     *
     * @return string
     */
    public function getRs4()
    {
        return $this->rs4;
    }

    /**
     * Set rs5
     *
     * @param string $rs5
     * @return Bussiness
     */
    public function setRs5($rs5)
    {
        $this->rs5 = $rs5;

        return $this;
    }

    /**
     * Get rs5
     *
     * @return string
     */
    public function getRs5()
    {
        return $this->rs5;
    }

    /**
     * Set showcase
     *
     * @param string $showcase
     * @return Bussiness
     */
    public function setShowcase($showcase)
    {
        $this->showcase = $showcase;

        return $this;
    }

    /**
     * Get showcase
     *
     * @return string
     */
    public function getShowcase()
    {
        return $this->showcase;
    }

    /**
     * Set outlet
     *
     * @param boolean $outlet
     * @return Bussiness
     */
    public function setOutlet($outlet)
    {
        $this->outlet = $outlet;

        return $this;
    }

    /**
     * Get outlet
     *
     * @return boolean
     */
    public function getOutlet()
    {
        return $this->outlet;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Bussiness
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Bussiness
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set status
     *
     * @param boolean $status
     * @return Bussiness
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set appmovil
     *
     * @param boolean $appmovil
     * @return Bussiness
     */
    public function setAppmovil($appmovil)
    {
        $this->appmovil = $appmovil;

        return $this;
    }

    /**
     * Get appmovil
     *
     * @return boolean
     */
    public function getAppmovil()
    {
        return $this->appmovil;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Bussiness
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set locality
     *
     * @param \Buscolook\WebBundle\Entity\Locality $locality
     * @return Bussiness
     */
    public function setLocality(\Buscolook\WebBundle\Entity\Locality $locality = null)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return \Buscolook\WebBundle\Entity\Locality
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set province
     *
     * @param \Buscolook\WebBundle\Entity\Province $province
     * @return Bussiness
     */
    public function setProvince(\Buscolook\WebBundle\Entity\Province $province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \Buscolook\WebBundle\Entity\Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set bussinesstype
     *
     * @param \Buscolook\WebBundle\Entity\BussinessType $bussinesstype
     * @return Bussiness
     */
    public function setBussinesstype(\Buscolook\WebBundle\Entity\BussinessType $bussinesstype)
    {
        $this->bussinesstype = $bussinesstype;

        return $this;
    }

    /**
     * Get bussinesstype
     *
     * @return \Buscolook\WebBundle\Entity\BussinessType
     */
    public function getBussinesstype()
    {
        return $this->bussinesstype;
    }

    /**
     * Add categories
     *
     * @param \Buscolook\WebBundle\Entity\Category $categories
     * @return Bussiness
     */
    public function addCategory(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->categories[] = $categories;

        return $this;
    }

    /**
     * Remove categories
     *
     * @param \Buscolook\WebBundle\Entity\Category $categories
     */
    public function removeCategory(\Buscolook\WebBundle\Entity\Category $categories)
    {
        $this->categories->removeElement($categories);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Get categories
     *
     * @param array \Doctrine\Common\Collections\Collection
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * Set user
     *
     * @param \Buscolook\WebBundle\Entity\User $user
     * @return Bussiness
     */
    public function setUser(\Buscolook\WebBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Buscolook\WebBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set target
     *
     * @param \Buscolook\WebBundle\Entity\Target $target
     * @return Bussiness
     */
    public function setTarget(\Buscolook\WebBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \Buscolook\WebBundle\Entity\Target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set highlight
     *
     * @param \Buscolook\WebBundle\Entity\Highlight $highlight
     * @return Bussiness
     */
    public function setHighlight(\Buscolook\WebBundle\Entity\Highlight $highlight = null)
    {
        $this->highlight = $highlight;

        return $this;
    }

    /**
     * Get highlight
     *
     * @return \Buscolook\WebBundle\Entity\Highlight
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * @return string
     */
    public function getFavs()
    {
        return $this->favs;
    }

    public function isFav($bussinessId, $userId)
    {
        $return = false;

        $favs = $this->favs;
        foreach ($favs as $fav) {
            if ($fav->getBussiness()->getId() == $bussinessId && $fav->getUser()->getId() == $userId) {
                return true;
            }
        }

        return $return;
    }

    /**
     * @param string $slogan
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    }

    /**
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * @return string
     */
    public function getShops()
    {
        return $this->favs;
    }

}