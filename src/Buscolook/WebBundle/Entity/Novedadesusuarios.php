<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Novedadesusuarios
 *
 * @ORM\Table(name="NovedadesUsuarios")
 * 
 */
class Novedadesusuarios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdUsuario", type="integer", nullable=true)
     */
    private $idusuario;

    /**
     * @var boolean
     *
     * @ORM\Column(name="TipoUsuario", type="boolean", nullable=true)
     */
    private $tipousuario;

    /**
     * @var string
     *
     * @ORM\Column(name="Texto", type="string", length=250, nullable=true)
     */
    private $texto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="Urlque", type="string", length=150, nullable=true)
     */
    private $urlque;

    /**
     * @var string
     *
     * @ORM\Column(name="Fans", type="text", nullable=true)
     */
    private $fans;


}
