<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stats
 *
 * @ORM\Table(name="stats")
 * //TODO: Importante esta tabla??
 */
class Stats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="op", type="string", length=255, nullable=false)
     */
    private $op;

    /**
     * @var string
     *
     * @ORM\Column(name="key2", type="string", length=255, nullable=false)
     */
    private $key2;

    /**
     * @var string
     *
     * @ORM\Column(name="uX", type="string", length=255, nullable=false)
     */
    private $ux;

    /**
     * @var string
     *
     * @ORM\Column(name="uY", type="string", length=255, nullable=false)
     */
    private $uy;

    /**
     * @var string
     *
     * @ORM\Column(name="REQUEST_TIME", type="string", length=255, nullable=false)
     */
    private $requestTime;

    /**
     * @var string
     *
     * @ORM\Column(name="HTTP_USER_AGENT", type="string", length=8000, nullable=false)
     */
    private $httpUserAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="QUERY_STRING", type="string", length=8000, nullable=false)
     */
    private $queryString;

    /**
     * @var string
     *
     * @ORM\Column(name="REMOTE_ADDR", type="string", length=255, nullable=false)
     */
    private $remoteAddr;

    /**
     * @var string
     *
     * @ORM\Column(name="idsColor", type="string", length=255, nullable=false)
     */
    private $idscolor;

    /**
     * @var string
     *
     * @ORM\Column(name="idsCategoria", type="string", length=255, nullable=false)
     */
    private $idscategoria;

    /**
     * @var string
     *
     * @ORM\Column(name="IdPuntoDeVenta", type="string", length=255, nullable=false)
     */
    private $idpuntodeventa;

    /**
     * @var string
     *
     * @ORM\Column(name="idPrenda", type="string", length=255, nullable=false)
     */
    private $idprenda;

    /**
     * @var string
     *
     * @ORM\Column(name="idTienda", type="string", length=255, nullable=false)
     */
    private $idtienda;

    /**
     * @var string
     *
     * @ORM\Column(name="PuntosVenta", type="string", length=255, nullable=false)
     */
    private $puntosventa;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=false)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=255, nullable=false)
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="macaddress", type="string", length=255, nullable=false)
     */
    private $macaddress;


}
