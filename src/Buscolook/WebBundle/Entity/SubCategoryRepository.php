<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class SubCategoryRepository extends EntityRepository
{
    public function findFilters()
    {
        $q = $this->createQueryBuilder('sc')
            ->select('sc.id,sc.name,sc.slug');

        return $q->getQuery()->getResult();
    }
}