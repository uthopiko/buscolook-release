<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class HighlightRepository extends EntityRepository
{
    public function findHomeGarments()
    {
        $q = $this->createQueryBuilder('h')
            ->where('h.area = :area')
            ->setParameter('area',Highlight::AREA_HOME)
            ->setMaxResults(3);

        return $q->getQuery()->getResult();
    }
}