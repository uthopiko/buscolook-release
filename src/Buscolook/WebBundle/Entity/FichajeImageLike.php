<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="fichaje_image_like")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FichajeImageLike
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FichajeImage", inversedBy="likes")
     * @ORM\JoinColumn(name="fichaje_image_id", referencedColumnName="id", nullable=false)
     */
    private $image;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param FichajeImage $image
     * @return $this
     */
    public function setImage(FichajeImage $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return FichajeImage
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }
}