<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Zone
 *
 * @ORM\Table(name="zone")
 * @ORM\Entity
 */
class Zone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="zone")
     * @ORM\JoinColumn(name="locality_id", referencedColumnName="id", nullable=false)
     */
    private $locality;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Zone
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locality
     *
     * @param \Buscolook\WebBundle\Entity\Locality $locality
     * @return Zone
     */
    public function setLocality(\Buscolook\WebBundle\Entity\Locality $locality)
    {
        $this->locality = $locality;
    
        return $this;
    }

    /**
     * Get locality
     *
     * @return \Buscolook\WebBundle\Entity\Locality 
     */
    public function getLocality()
    {
        return $this->locality;
    }
}