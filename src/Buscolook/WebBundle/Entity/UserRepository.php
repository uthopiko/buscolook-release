<?php
// src/Buscolook/WebBundle/Entity/UserRepository.php
namespace Buscolook\WebBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class UserRepository extends EntityRepository implements UserProviderInterface
{
    public function findBussinessByUser($id){
        $q = $this
            ->createQueryBuilder('b')
            ->select('u.id')
            ->from('BuscolookWebBundle:Bussiness','b')
            ->where('b.id = :id')
            ->setParameter('username', $id)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $bussiness = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find Bussiness identified by "%s".',
                $id
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }
    }

    public function findBuscolookers(){
        $q = $this
            ->createQueryBuilder('b')
            ->join('b.user_roles','r')
            ->join('b.fichajes','f')
            ->where('r.id = 6')
            ->groupBy('b.id')
            ->getQuery();
        $returnResult = [];
        foreach ($q->getResult() as $item){
            $images = $this->getRandomImages($item->getId());
            $returnResult [] = ['items'=>$item,'images'=>$images];

        }
        return $returnResult;
    }

    public function getRandomImages($userId) {
        $q = $this->createQueryBuilder('u')
            ->select('COUNT(i.id)')
            ->from('BuscolookWebBundle:FichajeImage','i')
            ->innerJoin('i.fichaje','f')
            ->innerJoin('f.buscolooker','b')
            ->where('b.id =:id')
            ->setParameter('id',$userId);

        $offset = rand(1,$q->getQuery()->getFirstResult());

        $q = $this->createQueryBuilder('u')
            ->select('i')
            ->from('BuscolookWebBundle:FichajeImage','i')
            ->innerJoin('i.fichaje','f')
            ->innerJoin('f.buscolooker','b')
            ->where('b.id =:id')
            ->setParameter('id',$userId)
            ->setFirstResult($offset)
            ->setMaxResults(2);

        return $q->getQuery()->getResult();

    }

    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin AcmeUserBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }
}