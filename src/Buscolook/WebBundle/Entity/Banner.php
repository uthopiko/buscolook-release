<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banners
 *
 * @ORM\Table(name="banner")
 * @ORM\Entity
 */
class Banner
{
    public static $positionArray = ["1" => "HOME","2" => "HOME_SUB"];
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string",length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="integer",nullable=true)
     */
    private $position;

    /**
     * @ORM\OneToMany(targetEntity="BannerMetadata", mappedBy="banner")
     */
    private $bannerMetadatas;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Banner
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $bannerMetadatas
     */
    public function setBannerMetadatas($bannerMetadatas)
    {
        $this->bannerMetadatas = $bannerMetadatas;
    }

    /**
     * @param mixed $bannerMetadatas
     */
    public function addBannerMetadata($bannerMetadata)
    {
        $this->bannerMetadatas [] = $bannerMetadata;
    }

    /**
     * @return mixed
     */
    public function getBannerMetadatas()
    {
        return $this->bannerMetadatas;
    }
}