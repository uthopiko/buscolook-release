<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class FichajeRepository extends EntityRepository
{
    public function findGarmentsByShop($shopId)
    {
        $q = $this->createQueryBuilder('g')
            ->join('g.shops', 's')
            ->where('s.id = :shopId')
            ->setParameter('shopId', $shopId);

        return $q->getQuery()->getResult();
    }

    public function findGarmentsByBussiness($bussinessId)
    {
        $q = $this->createQueryBuilder('g')
            ->join('g.shops', 's')
            ->join('s.bussiness','b')
            ->where('b.id = :bussinessId')
            ->setParameter('bussinessId', $bussinessId);

        return $q->getQuery()->getResult();
    }

    public function findHomeGarments()
    {
        $q = $this->createQueryBuilder('g')
            ->where('g.highlighted = 1')
            ->setMaxResults(3);

        return $q->getQuery()->getResult();
    }

    public function findGarmentHighlighted($category, $zona)
    {
        $q = $this->createQueryBuilder('g');
        if ($category != Category::ALL){
            $q->join('g.category', 'c')
              ->andWhere('c.slug = :category')
              ->setParameter('category',$category);
        }

        if ($zona != Locality::ALL) {
            $q->join('g.shops', 'sh')
              ->join('sh.locality','l')
              ->andWhere('l.slug = :zona')
              ->setParameter('zona',$zona);
        }
            $q->andWhere('g.highlighted = 1 AND g.active = 1');

        return $q->getQuery()->getResult();
    }

    public function findPublishedFichajes($category, $zona)
    {
        $q = $this->createQueryBuilder('f');
        /*if ($category != Category::ALL){
            $q->join('h.category', 'c')
                ->andWhere('c.slug = :category')
                ->setParameter('category',$category);
        }

        if ($zona != Locality::ALL) {
            $q->join('g.shops', 'sh')
                ->join('sh.locality','l')
                ->andWhere('l.slug = :zona')
                ->setParameter('zona',$zona);
        }*/
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        if ($category != Category::ALL){
            $q->andWhere('f.id IN (SELECT g.fichaje FROM Buscolook\WebBundle\Entity\Garment g INNER JOIN Buscolook\WebBundle\Entity\Category cat WHERE cat.slug = :category)')
            ->setParameter('category',$category);
        }

        return $q->getQuery()->getResult();
    }

    public function findFichajeByBussiness($bussinessId)
    {
        $q = $this->createQueryBuilder('f');
        $q->join('f.images','i');
        $q->where('f.bussiness = :bussiness');
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        $q->setParameter('bussiness',$bussinessId);


        return $q->getQuery()->getResult();
    }

    public function findFichajeByBuscolooker($buscolooker)
    {
        $q = $this->createQueryBuilder('f');
        $q->join('f.images','i');
        $q->where('f.buscolooker = :buscolooker');
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        $q->setParameter('buscolooker',$buscolooker);


        return $q->getQuery()->getResult();
    }

    public function findAllByBuscolooker($buscolooker) {
        $q = $this->createQueryBuilder('f');
        $q->join('f.garments','i');
        $q->where('f.buscolooker = :buscolooker');
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        $q->setParameter('buscolooker',$buscolooker);

        return $q->getQuery()->getResult();
    }

    public function getRandomImagesFichaje($fichajeId)
    {
        $q = $this->createQueryBuilder('m');
        $q->join('f.images','i');
        $q->where('f.buscolooker = :buscolooker');
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');
        $q->setParameter('buscolooker',$buscolooker);


        return $q->getQuery()->getResult();
    }
}