<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Shop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="shop")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=false)
     */
    private $bussiness;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Locality", inversedBy="bussiness")
     * @ORM\JoinColumn(name="locality_id", referencedColumnName="id", nullable=true)
     */
    private $locality;

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="bussiness")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=true)
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=12, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="Zona", type="string", length=255, nullable=true)
     */
    private $zona;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="schedule", type="string", length=255, nullable=true)
     */
    private $schedule;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Googlemap", type="string", length=255, nullable=true)
     */
    private $googlemap;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity="Fav", mappedBy="shop")
     */
    private $favs;

    /**
     * @ORM\ManyToMany(targetEntity="Garment", mappedBy="shops")
     */
    private $garments;

    public function __construct()
    {
        $this->garments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Shop
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Shop
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set zona
     *
     * @param string $zona
     * @return Shop
     */
    public function setZona($zona)
    {
        $this->zona = $zona;

        return $this;
    }

    /**
     * Get zona
     *
     * @return string
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Shop
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set schedule
     *
     * @param string $schedule
     * @return Shop
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return string
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Shop
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Shop
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set googlemap
     *
     * @param string $googlemap
     * @return Shop
     */
    public function setGooglemap($googlemap)
    {
        $this->googlemap = $googlemap;

        return $this;
    }

    /**
     * Get googlemap
     *
     * @return string
     */
    public function getGooglemap()
    {
        return $this->googlemap;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Shop
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Shop
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set bussiness
     *
     * @param \Buscolook\WebBundle\Entity\Bussiness $bussiness
     * @return Shop
     */
    public function setBussiness(\Buscolook\WebBundle\Entity\Bussiness $bussiness)
    {
        $this->bussiness = $bussiness;

        return $this;
    }

    /**
     * Get bussiness
     *
     * @return \Buscolook\WebBundle\Entity\Bussiness
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }

    /**
     * Set locality
     *
     * @param \Buscolook\WebBundle\Entity\Locality $locality
     * @return Shop
     */
    public function setLocality(\Buscolook\WebBundle\Entity\Locality $locality = null)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality
     *
     * @return \Buscolook\WebBundle\Entity\Locality
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set province
     *
     * @param \Buscolook\WebBundle\Entity\Province $province
     * @return Shop
     */
    public function setProvince(\Buscolook\WebBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \Buscolook\WebBundle\Entity\Province
     */
    public function getProvince()
    {
        return $this->province;
    }


    /**
     * @return string
     */
    public function getFavs()
    {
        return $this->favs;
    }

    public function isFav($shopId, $userId)
    {
        $return = false;

        $favs = $this->favs;
        foreach ($favs as $fav) {
            if ($fav->getShop()->getId() == $shopId && $fav->getUser()->getId() == $userId) {
                return true;
            }
        }

        return $return;
    }

    /**
     * Add garments
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garments
     * @return Garment
     */
    public function addGarment(\Buscolook\WebBundle\Entity\Garment $garments)
    {
        $this->garments[] = $garments;

        return $this;
    }

    /**
     * Remove garments
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garments
     */
    public function removeGarment(\Buscolook\WebBundle\Entity\Garment $garments)
    {
        $this->garments->removeElement($garments);
    }

    /**
     * Get garments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGarments()
    {
        return $this->garments;
    }
}