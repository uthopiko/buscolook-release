<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiscountTicket
 *
 * @ORM\Table(name="discount_ticket")
 * @ORM\Entity
 */
class DiscountTicket
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="discount_ticket")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=false)
     */
    private $bussiness;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="discount_ticket")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="sale_price", type="decimal", nullable=false)
     */
    private $salePrice;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="decimal", nullable=false)
     */
    private $discount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="emision_date", type="datetime", nullable=true)
     */
    private $emisionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_date", type="datetime", nullable=true)
     */
    private $expireDate;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="sale_price_trade", type="decimal", nullable=true)
     */
    private $salePriceTrade;

    /**
     * @var boolean
     *
     * @ORM\Column(name="trade", type="boolean", nullable=true)
     */
    private $trade;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trade_date", type="datetime", nullable=true)
     */
    private $tradeDate;

    //TODO: Generate set and getters

}
