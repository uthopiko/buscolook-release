<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * comment
 *
 * @ORM\Table(name="comment")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comment")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="Garment", inversedBy="comment")
     * @ORM\JoinColumn(name="garment_id", referencedColumnName="id", nullable=true)
     */
    private $garment;

    /**
     * @ORM\ManyToOne(targetEntity="Comment")
     * @ORM\JoinColumn(name="answer_to_comment_id", referencedColumnName="id", nullable=true)
     */
    private $answerTo;

    /**
     * @ORM\ManyToOne(targetEntity="Fichaje", inversedBy="comments")
     * @ORM\JoinColumn(name="fichaje_id", referencedColumnName="id", nullable=true)
     */
    private $fichaje;

    /**
     * @ORM\ManyToOne(targetEntity="Look", inversedBy="comment")
     * @ORM\JoinColumn(name="look_id", referencedColumnName="id", nullable=true)
     */
    private $look;

    /**
     * @ORM\ManyToOne(targetEntity="Bussiness", inversedBy="comment")
     * @ORM\JoinColumn(name="bussiness_id", referencedColumnName="id", nullable=true)
     */
    private $bussiness;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text",nullable=false)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="publish_in_wall", type="boolean", nullable=true)
     */
    private $publishInWall = false;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Comment
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Comment
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \Buscolook\WebBundle\Entity\User $user
     * @return Comment
     */
    public function setUser(\Buscolook\WebBundle\Entity\User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Buscolook\WebBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set garment
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garment
     * @return Comment
     */
    public function setGarment(\Buscolook\WebBundle\Entity\Garment $garment = null)
    {
        $this->garment = $garment;
    
        return $this;
    }

    /**
     * Get garment
     *
     * @return \Buscolook\WebBundle\Entity\Garment 
     */
    public function getGarment()
    {
        return $this->garment;
    }

    /**
     * Set look
     *
     * @param \Buscolook\WebBundle\Entity\Look $look
     * @return Comment
     */
    public function setLook(\Buscolook\WebBundle\Entity\Look $look = null)
    {
        $this->look = $look;
    
        return $this;
    }

    /**
     * Get look
     *
     * @return \Buscolook\WebBundle\Entity\Look 
     */
    public function getLook()
    {
        return $this->look;
    }

    /**
     * Set bussiness
     *
     * @param \Buscolook\WebBundle\Entity\Bussiness $bussiness
     * @return Comment
     */
    public function setBussiness(\Buscolook\WebBundle\Entity\Bussiness $bussiness = null)
    {
        $this->bussiness = $bussiness;
    
        return $this;
    }

    /**
     * Get bussiness
     *
     * @return \Buscolook\WebBundle\Entity\Bussiness 
     */
    public function getBussiness()
    {
        return $this->bussiness;
    }

    /**
     * @return Fichaje
     */
    public function getFichaje() {
        return $this->fichaje;
    }

    /**
     * @param Fichaje $fichaje
     */
    public function setFichaje(Fichaje $fichaje) {
        $this->fichaje = $fichaje;
    }

    public function publishInWall()
    {
        $this->publishInWall = true;
    }

    /**
     * @return boolean
     */
    public function isPublishedInWall()
    {
        return $this->publishInWall;
    }

    /**
     * @param Comment $answerTo
     */
    public function setAnswerTo(Comment $answerTo)
    {
        $this->answerTo = $answerTo;
    }

    /**
     * @return Comment
     */
    public function getAnswerTo()
    {
        return $this->answerTo;
    }

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->setCreatedAt(new \DateTime());
    }
}