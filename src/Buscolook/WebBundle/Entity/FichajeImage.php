<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * FichajeImage
 *
 * @ORM\Table(name="fichaje_image")
 * @ORM\Entity
 */
class FichajeImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Fichaje", inversedBy="fichaje_image")
     * @ORM\JoinColumn(name="fichaje_id", referencedColumnName="id", nullable=false)
     */
    private $fichaje;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="FichajeImageLike", mappedBy="image")
     */
    private $likes;

    public function __construct() {
        $this->likes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return FichajeImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return FichajeImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    
        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set fichaje
     *
     * @param \Buscolook\WebBundle\Entity\Fichaje $fichaje
     * @return FichajeImage
     */
    public function setFichaje(\Buscolook\WebBundle\Entity\Fichaje $fichaje)
    {
        $this->fichaje = $fichaje;
    
        return $this;
    }

    /**
     * Get fichaje
     *
     * @return \Buscolook\WebBundle\Entity\Fichaje 
     */
    public function getFichaje()
    {
        return $this->fichaje;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return FichajeImageLike
     */
    public function getLikes() {
        return $this->likes;
    }

    /**
     * @param FichajeImageLike[] $likes
     */
    public function setLikes(array $likes) {
        $this->likes = $likes;
    }

    /**
     * @param FichajeImageLike $like
     * @return $this
     */
    public function addLike(FichajeImageLike $like) {
        if (!$this->likes->contains($like)) {
            $this->likes->add($like);
            $like->setImage($this);
        }

        return $this;
    }
}