<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class FichajeImageRepository extends EntityRepository
{
    public function getRandomImagesFichaje($fichajeId)
    {
        $q = $this->createQueryBuilder('i');
        $q->where('i.fichaje = :fichaje');
        $q->setParameter('fichaje',$fichajeId);


        return $q->getQuery()->getResult();
    }
}