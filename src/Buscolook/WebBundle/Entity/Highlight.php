<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Highlight
 *
 * @ORM\Table(name="highlight")
 * @ORM\Entity(repositoryClass="Buscolook\WebBundle\Entity\HighlightRepository")
 */
class Highlight
{
    const AREA_HOME="Home";
    const AREA_COTILLEO="Cotilleo";
    const AREA_INSPIRO="Inspiro";
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="highlight")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id", nullable=true)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="Garment", inversedBy="highlight")
     * @ORM\JoinColumn(name="garment_id", referencedColumnName="id", nullable=true)
     */
    private $garment;

    /**
     * @var string
     *
     * @ORM\Column(name="Area", type="string", length=255, nullable=true)
     */
    private $area;

    /**
     * @ORM\ManyToOne(targetEntity="Province", inversedBy="highlight")
     * @ORM\JoinColumn(name="province_id", referencedColumnName="id", nullable=true)
     */
    private $province;

    
    /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="highlight")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=true)
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin_date", type="datetime", nullable=true)
     */
    private $beginDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="observations", type="string", length=255, nullable=true)
     */
    private $observations;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return Highlight
     */
    public function setArea($area)
    {
        $this->area = $area;
    
        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set beginDate
     *
     * @param \DateTime $beginDate
     * @return Highlight
     */
    public function setBeginDate($beginDate)
    {
        $this->beginDate = $beginDate;
    
        return $this;
    }

    /**
     * Get beginDate
     *
     * @return \DateTime 
     */
    public function getBeginDate()
    {
        return $this->beginDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Highlight
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set observations
     *
     * @param string $observations
     * @return Highlight
     */
    public function setObservations($observations)
    {
        $this->observations = $observations;
    
        return $this;
    }

    /**
     * Get observations
     *
     * @return string 
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Set shop
     *
     * @param \Buscolook\WebBundle\Entity\Shop $shop
     * @return Highlight
     */
    public function setShop(\Buscolook\WebBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;
    
        return $this;
    }

    /**
     * Get shop
     *
     * @return \Buscolook\WebBundle\Entity\Shop 
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Set garment
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garment
     * @return Highlight
     */
    public function setGarment(\Buscolook\WebBundle\Entity\Garment $garment = null)
    {
        $this->garment = $garment;
    
        return $this;
    }

    /**
     * Get garment
     *
     * @return \Buscolook\WebBundle\Entity\Garment 
     */
    public function getGarment()
    {
        return $this->garment;
    }

    /**
     * Set province
     *
     * @param \Buscolook\WebBundle\Entity\Province $province
     * @return Highlight
     */
    public function setProvince(\Buscolook\WebBundle\Entity\Province $province = null)
    {
        $this->province = $province;
    
        return $this;
    }

    /**
     * Get province
     *
     * @return \Buscolook\WebBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set category
     *
     * @param \Buscolook\WebBundle\Entity\Category $category
     * @return Highlight
     */
    public function setCategory(\Buscolook\WebBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Buscolook\WebBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }
}