<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 11/3/13
 * Time: 1:53 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Entity;


use Doctrine\ORM\EntityRepository;

class BussinessRepository extends EntityRepository
{
    public function findActiveShops($category,$zona)
    {
        $q = $this->createQueryBuilder('b')
            ->select('b')
            ->join('b.shops','s')
            ->join('s.garments','g');

        $q->join('g.fichaje','f');
        if ($category != Category::ALL){
            $q->join('g.subcategory', 'c')
                ->andWhere('c.slug = :category')
                ->setParameter('category',$category);
        }

        if ($zona != Locality::ALL) {
            $q->join('g.shops', 'sh')
                ->join('sh.locality','l')
                ->andWhere('l.slug = :zona')
                ->setParameter('zona',$zona);
        }
        $q->andWhere('f.published = 1 AND f.deletedAt IS NULL');

        return $q->getQuery()->getResult();
    }
}