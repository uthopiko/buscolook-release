<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GarmentImage
 *
 * @ORM\Table(name="garment_image")
 * @ORM\Entity
 */
class GarmentImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Garment", inversedBy="garment_image")
     * @ORM\JoinColumn(name="garment_id", referencedColumnName="id", nullable=false)
     */
    private $garment;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var boolean 
     *
     * @ORM\Column(name="principal", type="boolean")
     */
    private $principal;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return GarmentImage
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set principal
     *
     * @param boolean $principal
     * @return GarmentImage
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    
        return $this;
    }

    /**
     * Get principal
     *
     * @return boolean 
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set garment
     *
     * @param \Buscolook\WebBundle\Entity\Garment $garment
     * @return GarmentImage
     */
    public function setGarment(\Buscolook\WebBundle\Entity\Garment $garment)
    {
        $this->garment = $garment;
    
        return $this;
    }

    /**
     * Get garment
     *
     * @return \Buscolook\WebBundle\Entity\Garment 
     */
    public function getGarment()
    {
        return $this->garment;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}