<?php

namespace Buscolook\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Propuestas
 * TODO: Es necesaria??No tiene nada...
 * @ORM\Table(name="Propuestas")
 * 
 */
class Propuestas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nombre", type="string", length=20, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="Descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="idTienda", type="integer", nullable=true)
     */
    private $idtienda;

    /**
     * @var string
     *
     * @ORM\Column(name="Imagen", type="string", length=20, nullable=true)
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="Opcion1", type="string", length=255, nullable=true)
     */
    private $opcion1;

    /**
     * @var string
     *
     * @ORM\Column(name="Imagen1", type="string", length=255, nullable=true)
     */
    private $imagen1;

    /**
     * @var string
     *
     * @ORM\Column(name="Opcion2", type="string", length=255, nullable=true)
     */
    private $opcion2;

    /**
     * @var string
     *
     * @ORM\Column(name="Imagen2", type="string", length=255, nullable=true)
     */
    private $imagen2;

    /**
     * @var string
     *
     * @ORM\Column(name="Opcion3", type="string", length=255, nullable=true)
     */
    private $opcion3;

    /**
     * @var string
     *
     * @ORM\Column(name="Imagen3", type="string", length=255, nullable=true)
     */
    private $imagen3;

    /**
     * @var string
     *
     * @ORM\Column(name="Votos", type="string", length=255, nullable=true)
     */
    private $votos;

    /**
     * @var string
     *
     * @ORM\Column(name="Premio", type="string", length=255, nullable=true)
     */
    private $premio;

    /**
     * @var string
     *
     * @ORM\Column(name="Condiciones", type="string", length=20, nullable=true)
     */
    private $condiciones;


}
