<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\AdminBundle\Form\GarmentType;
use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Entity\Fichaje;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\WebBundle\Entity\Inspiration;
use Buscolook\WebBundle\Entity\User;
use Buscolook\WebBundle\Form\BuscolookerProductType;
use Buscolook\WebBundle\Form\ProductType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GarmentController extends BaseCrudController
{
    public function getViewPath()
    {
        return 'Garment';
    }

    public function getEntityClass()
    {
        return new Garment();
    }

    public function getEntityType()
    {
        return new GarmentType($this->getRequest()->attributes->get('parentId'), $this->getEntityManager(), 'Shop');
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Garment');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }

    public function cotilleoAction (Request $request, $category, $type, $zona)
    {
        $em = $this->getDoctrine()->getManager();
        $search = $request->request->get('search',null);

        $garments = $em->getRepository('BuscolookWebBundle:Garment')->findPublishedFichajes($category, $zona, $search);

        return $this->render('BuscolookWebBundle:Garment:cotilleo.html.twig',
                                [
                                    'fichajes'=>$garments,
                                    'category' => $category,
                                    'zona' => $zona
                                ]
        );
    }

    public function inspiroAction (Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $inspirations = $em->getRepository('BuscolookWebBundle:Inspiration')->findBy(['published'=>true]);

        return $this->render('BuscolookWebBundle:Garment:inspiro.html.twig', ['inspirations' => $inspirations]);
    }

    public function inspiroDetailsAction (Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $inspiration = $em->getRepository('BuscolookWebBundle:Inspiration')->findOneBy(['id'=>$id,'deletedAt'=>null]);

        return $this->render('BuscolookWebBundle:Garment:inspiro-details.html.twig',['inspiration' => $inspiration]);
    }

    public function bussinessGarmentAction(Request $request, Bussiness $bussinessId, Fichaje $fichajeId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ProductType($bussinessId), new Garment());
        if ($request->isXmlHttpRequest()) {
            if($request->getMethod()=='POST') {
                $form->handleRequest($request);
                if($form->isValid()) {
                    if($fichajeId == null) {
                        $fichajeId = new Fichaje();
                        $fichajeId->setBussiness($bussinessId);
                        $em->persist($fichajeId);
                        $form->getData()->setFichaje($fichajeId);
                    }
                    else {
                        $fichajeId->setName($request->request->get('buscolook_webbundle_garment')['fichaje_name']);
                        $form->getData()->setFichaje($fichajeId);
                    }
                    $em->persist($form->getData());
                    $em->flush();
                    return new JsonResponse(['fichaje'=>['id'=>$fichajeId->getId()]],200);
                }
            }
        }

        return new JsonResponse(['Error'],400);
    }

    public function buscolookerGarmentAction(Request $request, Fichaje $fichajeId = null, User $buscolooker)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new BuscolookerProductType(), new Garment());
        if ($request->isXmlHttpRequest()) {
            if($request->getMethod()=='POST') {
                $form->handleRequest($request);
                if($form->isValid()) {
                    if($fichajeId == null) {
                        $fichajeId = new Fichaje();
                        $fichajeId->setName('blk'.uniqid());
                        $fichajeId->setBuscolooker($buscolooker);
                        $em->persist($fichajeId);
                        $form->getData()->setFichaje($fichajeId);
                    }
                    else {
                        $fichajeId->setName($request->request->get('buscolook_webbundle_garment')['fichaje_name']);
                        $form->getData()->setFichaje($fichajeId);
                    }
                    $em->persist($form->getData());
                    $em->flush();
                    return new JsonResponse(['fichaje'=>['id'=>$fichajeId->getId()]],200);
                }
            }
        }

        return new JsonResponse(['Error'],400);
    }
}