<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\AdminBundle\Form\RegistrationType;
use Buscolook\WebBundle\Form\BussinessProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ProfileController extends Controller
{
    public function accountAction(Request $request)
    {
        /*$em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm(new RegistrationType(), $user);

        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->flush();
            }
        }*/
        return $this->render('BuscolookWebBundle:Profile:index.html.twig'/*, ['form'=>$form->createView()]*/);
    }

    public function profileBussinessAction(Request $request)
    {
        /*$em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $bRep = $em->getRepository('BuscolookWebBundle:Bussiness');
        $bussiness = $em->getRepository('BuscolookWebBundle:Bussiness')->findOneByUser($user->getId());
        $form = $this->createForm(new BussinessProfileType(), $bussiness, array('action'=>'#'));

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($bussiness === null) {
                $bussiness = $form->getData();
                $bussiness->setUser($user);
                $em->persist($bussiness);
            }

            $em->flush();
        }*/
        return $this->render('BuscolookWebBundle:Profile:index.html.twig'/*, ['form'=>$form->createView()]*/);
    }

    public function profileAction(Request $request)
    {
        return $this->render('BuscolookWebBundle:Profile:index.html.twig'/*, ['form'=>$form->createView()]*/);
    }

    public function accountBussinessAction(Request $request)
    {

    }
}
