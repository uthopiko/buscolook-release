<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Form\ShopType;
use Buscolook\WebBundle\Entity\Shop;
use Symfony\Component\HttpFoundation\Request;

class ShopController extends BaseController
{
    public function postShopAction (Request $request, Bussiness $bussiness)
    {
        $em = $this->getDoctrine()->getManager();
        $shopForm = $this->createForm(new ShopType(),  new Shop());

        $shopForm->handleRequest($request);

        if ($request->getMethod() == "POST") {
            if ($shopForm->isValid()) {
                $shopForm->getData()->setBussiness($bussiness);
                $em->persist($shopForm->getData());
                $em->flush();

                return $this->redirect($this->generateUrl('buscolook_web_shop_public_profile',['bussinessId'=>$bussiness->getId()]));
            }
        }
        return $this->redirect($this->generateUrl('buscolook_web_shop_public_profile',['bussinessId'=>$bussiness->getId()]));
    }


}