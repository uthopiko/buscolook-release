<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\AdminBundle\Form\CategoryType;
use Buscolook\WebBundle\Entity\Category;
use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Symfony\Component\BrowserKit\Request;

class CategoryController extends BaseCrudController
{
    public function getViewPath()
    {
        return 'Category';
    }

    public function getEntityClass()
    {
        return new Category();
    }

    public function getEntityType()
    {
        return new CategoryType();
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Category');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }
}
