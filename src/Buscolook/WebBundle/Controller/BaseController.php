<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\GarmentRepository;
use Buscolook\WebBundle\Entity\HighlightRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * Gets a repository by name
     * @param string $name
     * @return mixed
     */
    protected function getRepository($name)
    {
        return $this->getDoctrine()->getRepository('BuscolookWebBundle:' . $name);
    }

    /**
     * Gets a repository by name
     * @param string $name
     * @return GarmentRepository
     */
    protected function getRepositoryGarment()
    {
        return $this->getDoctrine()->getRepository('BuscolookWebBundle:Garment');
    }

    /**
     * Gets a repository by name
     * @param string $name
     * @return BussinessRepository
     */
    protected function getRepositoryBussiness()
    {
        return $this->getDoctrine()->getRepository('BuscolookWebBundle:Bussiness');
    }

    /**
     * Gets a repository by name
     * @param string $name
     * @return ShopRepository
     */
    protected function getRepositoryShop()
    {
        return $this->getDoctrine()->getRepository('BuscolookWebBundle:Shop');
    }

    /**
     * Gets a repository by name
     * @param string $name
     * @return HighlightRepository
     */
    protected function getRepositoryHighlight()
    {
        return $this->getDoctrine()->getRepository('BuscolookWebBundle:Highlight');
    }

    /**
     * Gets the KNP Paginator instance
     * @return Paginator
     */
    protected function getPaginator()
    {
        return $this->get('knp_paginator');
    }

    /**
     * @param string $parameterName
     * @return string
     */
    protected function getParameter($parameterName)
    {
        return $this->container->getParameter($parameterName);
    }

    /**
     * Shortcut to obtain the entity manager instance
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Gets if there is an active user logged in currently
     * @return bool
     */
    public function isUserLoggedIn()
    {
        $user = $this->getUser();

        return null !== $user;
    }
}