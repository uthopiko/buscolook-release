<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Banner;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Buscolook\WebBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Aws\Common\Aws;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

class DefaultController extends BaseController
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();

        $categories = $em->getRepository('BuscolookWebBundle:Category')->findByType(Category::ARTICLE);
        $principalBannerImages = $em->getRepository('BuscolookWebBundle:BannerMetadata')->findBannerImages(1);
        $subhomeBannerImages = $em->getRepository('BuscolookWebBundle:BannerMetadata')->findBannerImages(2);

        return $this->render('BuscolookWebBundle:Default:index.html.twig',array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'categories'    => $categories,
                'principal_banner_images' => $principalBannerImages,
                'subhome_banner_images' => $subhomeBannerImages
            ));
    }

    public function renderFiltersAction (Request $request, $entity, $category = null, $zona = null, $url = 'buscolook_web_cotilleo') {
        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('BuscolookWebBundle:' . $entity);

        $results = $repository->findFilters();

        return $this->render('BuscolookWebBundle:Default:filter.html.twig',['results'=>$results,'selectedCategory'=> $category, 'selectedZone' => $zona, 'url' => $url]);

    }

    public function helloAction($name)
    {
    	$client = S3Client::factory(array(
		    'key'    => 'AKIAIWUYVYJMAF3YDOZQ',
		    'secret' => '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',
		));

        $result = $client->putObject(array(
            'Bucket' => 'buscolook',
            'Key'    => 'asss/data.txt',
            'Body'   => 'Hello!'
        ));

        var_dump($result);

        return array('name' => $name);
    }

    /**
     * Shows a static page
     * @param $page
     * @return Response
     */
    public function staticAction($page) {
        return $this->render(sprintf('BuscolookWebBundle:Static:%s.html.twig', $page));
    }
}
