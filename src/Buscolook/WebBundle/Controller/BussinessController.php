<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Shop;
use Buscolook\WebBundle\Form\BussinessLogoType;
use Buscolook\WebBundle\Form\ShopType;
use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\WebBundle\Entity\Fichaje;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\WebBundle\Form\BussinessAccountType;
use Buscolook\WebBundle\Form\BussinessProfileType;
use Buscolook\WebBundle\Form\ProductType;
use Buscolook\WebBundle\Helpers\FormErrorHelper;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BussinessController extends BaseController
{
    public function getViewPath()
    {
        return 'Bussiness';
    }

    public function getEntityClass()
    {
        return new Bussiness();
    }

    public function getEntityType()
    {
        return new BussinessProfileType();
    }

    public function getEntityRepository()
    {
        return $this->getRepository('Bussiness');
    }

    public function getAll()
    {
        return $this->getEntityRepository()->findAll();
    }

    public function shopsAction(Request $request, $category, $type, $zona)
    {
        $bussiness = $this->getRepositoryBussiness()->findAll();

        $em = $this->getDoctrine()->getManager();

        $shops = $em->getRepository('BuscolookWebBundle:Bussiness')->findActiveShops($category, $zona);

        return $this->render('BuscolookWebBundle:Bussiness:index.html.twig',
            [
                'shops'=>$shops,
                'category' => $category,
                'zona' => $zona
            ]
        );
        return $this->render('BuscolookWebBundle:Bussiness:index.html.twig',['shops' => $bussiness]);
    }

    public function updateLogoAction(Request $request, Bussiness $bussiness) {
        $em = $this->getDoctrine()->getManager();
        $bussinessLogoForm = $this->createForm(new BussinessLogoType(),  $bussiness);

        $bussinessLogoForm->handleRequest($request);

        if($request->getMethod() == "POST") {
            if ($bussinessLogoForm->isValid()) {
                  $em->persist($bussinessLogoForm->getData());
                  $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('buscolook_web_shop_public_profile',['bussinessId' => $bussiness->getId()]));
    }

    public function publicProfileAction(Request $request, $bussiness)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new ProductType($bussiness), new Garment());
        $shop = $this->getRepositoryBussiness()->find($bussiness);
        $sc = $this->container->get('security.context');
        if (!$sc->isGranted('ROLE_BUSCOLOOKER')) {
            $garments = $this->getRepository('Fichaje')->findFichajeByBussiness($bussiness);
        }
        else {
            $garments = $this->getRepository('Fichaje')->findBy(['bussiness'=>$bussiness]);

        }
        $favorites = $this->getRepository('Fav')->findFavouritesByUser($shop->getUser()->getId());
        $following = $this->getRepository('Fav')->findFollowingByUser($shop->getUser()->getId());
        $profileForm = $this->createForm(new BussinessAccountType(),  $shop);
        $shopForm = $this->createForm(new ShopType(),  new Shop());
        $bussinessLogoForm = $this->createForm(new BussinessLogoType(),  $shop);

        $shopForm->handleRequest($request);
        $bussinessLogoForm->handleRequest($request);
        $profileForm->handleRequest($request);

        if($request->getMethod() == "POST") {

            if ($profileForm->isValid()) {
                $em->persist($profileForm->getData());
                $em->flush();
            }
            else {
                $errorHelper = new FormErrorHelper();
                $errors = $errorHelper->getFormErrors($profileForm);
            }


        }
        return $this->render('BuscolookWebBundle:Bussiness:publicProfile.html.twig',
            [
                'form'=>$form->createView(),
                'profile_form'=>$profileForm->createView(),
                'shop_form'=>$shopForm->createView(),
                'logo_form'=>$bussinessLogoForm->createView(),
                'bussinessEntity'=>$shop,
                'garments'=>$garments,
                'following'=>$following,
                'favourites'=>$favorites]);
    }

    public function profileAction(Request $request, $bussiness)
    {
        $em = $this->getDoctrine()->getManager();
        $bussinessEntity = $em->getRepository('BuscolookWebBundle:Bussiness')->find($bussiness);
        $fichajes = $em->getRepository('BuscolookWebBundle:Fichaje')->findFichajeByBussiness($bussiness);
        return $this->render('BuscolookWebBundle:Buscolooker:profile.html.twig',['bussiness'=>$bussinessEntity,'fichajes'=>$fichajes]);
    }

    public function productPopupsAction(Request $request, Fichaje $fichajeId = null,Bussiness $bussinessId = null)
    {

        $garments = array();
        if($fichajeId) {
            $garments = $this->getRepositoryGarment()->findBy(['fichaje'=>$fichajeId]);
        }
        $form = $this->createForm(new ProductType($bussinessId), new Garment());

        return $this->render('BuscolookWebBundle:Bussiness:Include/addproduct.html.twig',
            [
                'fichajeId'=>$fichajeId,
                'bussinessEntity'=>$bussinessId,
                'form'=>$form->createView(),
                'garments'=>$garments
            ]
        );
    }

    public function getShopsAction(Request $request, $bussiness){
        $em = $this->getDoctrine()->getManager();

        $shops = $em->getRepository('BuscolookWebBundle:Shop')->findBy(['bussiness'=>$bussiness]);

        $returnArray = null;
        foreach ($shops as $shop) {
            $returnArray [] = ['id'=>$shop->getId(),'name'=>$shop->getName()];
        }

        return new JsonResponse($returnArray, 200);
    }


}