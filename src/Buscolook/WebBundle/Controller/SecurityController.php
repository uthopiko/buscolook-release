<?php
// src/Buscolook/WebBundle/Controller/SecurityController.php;
namespace Buscolook\WebBundle\Controller;

use Buscolook\AdminBundle\Form\RegistrationFinalStepType;
use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Entity\Locality;
use Buscolook\WebBundle\Entity\Shop;
use Buscolook\WebBundle\Helpers\FormErrorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;
use Buscolook\WebBundle\Entity\User;
use Buscolook\AdminBundle\Form\RegistrationType;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager;

class SecurityController extends Controller
{
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
                    $error = $request->attributes->get(
                        SecurityContext::AUTHENTICATION_ERROR
                    );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'BuscolookWebBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }

    public function registerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $registration = new User();
        $form = $this->createForm(new RegistrationType(), $registration, array('action'=>'#'));

        if ($request->getMethod() === 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $user = $this->get('blk.user_manager')->createUser($form->getData(), ['ROLE_USER','ROLE_BUSCOLOOKER']);

                $providerKey = 'user_provider';
                $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

                $this->container->get('security.context')->setToken($token);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Empresa registrada correctamente'
                );

                return $this->redirect($this->generateUrl('buscolook_homepage'));

            }
        }


        return $this->render(
            'BuscolookWebBundle:Security:register.html.twig',
            array('form' => $form->createView())
        );
    }

    public function registerBussinessAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $registration = new User();
        $form = $this->createForm(new RegistrationType(), $registration, array('action'=>'#'));
        $errors = null;
        if ($request->getMethod() === 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $bussiness = $this->get('blk.user_manager')->createUser($form->getData(), ['ROLE_USER','ROLE_BUSSINESS'], 1);

                $session->set('bussiness_id',$bussiness->getId());
                $providerKey = 'user_provider';
                $token = new UsernamePasswordToken($bussiness->getUser(), null, $providerKey, $bussiness->getUser()->getRoles());

                $this->container->get('security.context')->setToken($token);

                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Empresa registrada correctamente'
                );

                return $this->redirect($this->generateUrl('buscolook_register_bussiness_second_step'));
            }

        }


        return $this->render(
            'BuscolookWebBundle:Security:register-bussiness.html.twig',
            array('form' => $form->createView())
        );
    }

    public function registerBussinesStepTwoAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $bussinessId = $session->get('bussiness_id');
        $registration = $em->getRepository('BuscolookWebBundle:Bussiness')->find($bussinessId);
        $form = $this->createForm(new \Buscolook\WebBundle\Form\RegistrationFinalStepType(), $registration, array('action'=>'#'));

        if ($request->getMethod() === 'POST') {
            $form->submit($request);
            if ($form->isValid()) {
                $bussiness  = $form->getData();
                $shop = new Shop();
                $locality = $this->getLocalityEntity($request->get('locality'));
                $bussiness->setLocality($locality);
                $shop->setLocality($locality);
                $shop->setBussiness($bussiness);
                $em->persist($bussiness);
                $em->persist($shop);
                $em->flush();

                return $this->redirect($this->generateUrl('buscolook_homepage'));
            }

        }


        return $this->render(
            'BuscolookWebBundle:Security:register-bussiness-2.html.twig',
            array('form' => $form->createView())
        );
    }

    public function getLocalityEntity($locality){
        $em = $this->getDoctrine()->getManager();

        $localityEntity = $em->getRepository('BuscolookWebBundle:Locality')->findOneBy(['locality' => $locality]);
        if ($localityEntity) {
            return $localityEntity;
        }

        $locality = $this->createLocality($locality);
        return $locality;
    }

    public function createLocality($localityStr) {
        $em = $this->getDoctrine()->getManager();

        $locality = new Locality();
        $locality->setLocality($localityStr);
        $locality->setSlug(strtolower($localityStr));
        $locality->setProvince(1);
        $locality->setVerified(false);

        $em->persist($locality);
        $em->flush();

        return $locality;
    }
}