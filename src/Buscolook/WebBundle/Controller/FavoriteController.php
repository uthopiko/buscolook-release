<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Entity\Fav;
use Buscolook\WebBundle\Entity\Fichaje;
use Buscolook\WebBundle\Entity\Shop;
use Buscolook\WebBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FavoriteController extends BaseController
{
    public function shopAction(Request $request, Shop $shopId)
    {

    }

    public function accountAction()
    {

    }

    public function addFollowerAction(Request $request,Bussiness $bussinessId)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $fav = new Fav();
        $fav->setBussiness($bussinessId);
        $fav->setUser($user);
        $fav->setCreatedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($fav);
        $em->flush();

        return new JsonResponse(json_encode($fav),200);
    }

    public function removeFollowerAction(Bussiness $bussinessId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $favorites = $em->getRepository('BuscolookWebBundle:Fav')->findBy(['bussiness' => $bussinessId->getId(),'user' => $user->getId()]);
        foreach($favorites as $fav){
            $em->remove($fav);
        }

        $em->flush();

        return new JsonResponse(json_encode($fav),200);
    }

    public function addFavouriteAction(Request $request,Fichaje $fichaje)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $fav = new Fav();
        $fav->setFichaje($fichaje);
        $fav->setUser($user);
        $fav->setCreatedAt(new \DateTime('now'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($fav);
        $em->flush();

        return new JsonResponse(json_encode($fav),200);
    }

    public function removeFavouriteAction(Fichaje $fichaje)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $favorites = $em->getRepository('BuscolookWebBundle:Fav')->findBy(['fichaje' => $fichaje->getId(),'user' => $user->getId()]);
        foreach($favorites as $fav){
            $em->remove($fav);
        }

        $em->flush();

        return new JsonResponse('',200);
    }

}
