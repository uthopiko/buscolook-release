<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\AdminBundle\Controller\Crud\BaseCrudController;
use Buscolook\WebBundle\Entity\Fichaje;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\WebBundle\Entity\User;
use Buscolook\WebBundle\Form\BuscolookerAccountType;
use Buscolook\WebBundle\Form\BuscolookerLogoType;
use Buscolook\WebBundle\Form\BuscolookerProductType;
use Buscolook\WebBundle\Form\BussinessProfileType;
use Buscolook\WebBundle\Form\ProductType;
use Buscolook\WebBundle\Helpers\FormErrorHelper;
use Symfony\Component\HttpFoundation\Request;

class BuscolookerController extends BaseController
{
    public function getViewPath()
    {
        return 'Buscolooker';
    }

    public function getEntityClass()
    {
        return new User();
    }

    public function getEntityType()
    {
        return new BussinessProfileType();
    }

    public function getEntityRepository()
    {
        return $this->getRepository('User');
    }


    public function buscolookersAction(Request $request)
    {
        $buscolookers = $this->getEntityRepository()->findBuscolookers();

        return $this->render('BuscolookWebBundle:Buscolooker:index.html.twig',['buscolookers' => $buscolookers]);
    }

    public function publicProfileAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        //TODO: ADD ShopSlug instead $shopId
        $form = $this->createForm(new BuscolookerProductType(), new Garment());
        $profileForm = $this->createForm(new BuscolookerAccountType(),  $user);
        $buscolookerLogoForm = $this->createForm(new BuscolookerLogoType(),  $user);

        $sc = $this->container->get('security.context');
        if (!$sc->isGranted('ROLE_BUSCOLOOKER')) {
            $garments = $this->getRepository('Fichaje')->findFichajeByBuscolooker($user->getId());
        }
        else {
            $garments = $this->getRepository('Fichaje')->findAllByBuscolooker($user->getId());
        }
        $favorites = $this->getRepository('Fav')->findFavouritesByUser($user->getId());
        $following = $this->getRepository('Fav')->findFollowingByUser($user->getId());

        $profileForm->handleRequest($request);
        $buscolookerLogoForm->handleRequest($request);

        if($request->getMethod() == "POST") {

            if ($profileForm->isValid()) {
                $em->persist($profileForm->getData());
                $em->flush();
            }
            else {
                $errorHelper = new FormErrorHelper();
                $errors = $errorHelper->getFormErrors($profileForm);
            }

            if ($buscolookerLogoForm->isValid()) {
                $em->persist($buscolookerLogoForm->getData());
                $em->flush();
            }
            else {
                $errorHelper = new FormErrorHelper();
                $errors = $errorHelper->getFormErrors($buscolookerLogoForm);
            }
        }
        return $this->render('BuscolookWebBundle:Buscolooker:publicProfile.html.twig',
            [
                'form'=>$form->createView(),
                'user'=>$user,
                'garments'=>$garments,
                'following'=>$following,
                'favourites'=>$favorites,
                'profile_form'=>$profileForm->createView(),
                'logo_form'=>$buscolookerLogoForm->createView()]);
    }

    public function profileAction(Request $request)
    {
        return $this->render('BuscolookWebBundle:Buscolooker:profile.html.twig');
    }

    public function productPopupsAction(Request $request, Fichaje $fichajeId = null,User $buscolooker = null)
    {
        $garments = array();
        if($fichajeId) {
            $garments = $this->getRepositoryGarment()->findBy(['fichaje'=>$fichajeId]);
        }
        $form = $this->createForm(new BuscolookerProductType(), new Garment());

        return $this->render('BuscolookWebBundle:Buscolooker:Include/addproduct_buscolooker.html.twig',
            [
                'fichajeId'=>$fichajeId,
                'buscolooker'=>$buscolooker->getId(),
                'form'=>$form->createView(),
                'garments'=>$garments
            ]
        );
    }
}