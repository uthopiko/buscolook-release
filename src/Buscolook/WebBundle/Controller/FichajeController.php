<?php

namespace Buscolook\WebBundle\Controller;

use Buscolook\WebBundle\Entity\Bussiness;
use Buscolook\WebBundle\Entity\Comment;
use Buscolook\WebBundle\Entity\Fichaje;
use Buscolook\WebBundle\Entity\FichajeImage;
use Buscolook\WebBundle\Entity\Garment;
use Buscolook\WebBundle\Entity\User;
use Buscolook\WebBundle\Form\FichajeImageType;
use Buscolook\WebBundle\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FichajeController extends Controller
{
    public function addImageAction(Request $request, Fichaje $fichajeId)
    {
        $image = new FichajeImage();
        $imageForm = $this->createForm(new FichajeImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $files = $request->files->get('files');
            $numImages = 0;
            foreach ($files as $file) {
                $filename = uniqid();
                $arrKey = $file->getClientOriginalName();
                $size = $file->getClientSize();
                $mimeType = $file->getMimeType();
                $fileReturn = $uploadManager->uploadFile([$file], $fichajeId, 'fichaje', $filename);
                $numImages++;
            }

            $response = new JsonResponse(['msg' => 'Se ' . ($numImages > 1 ? 'han' : 'ha') . ' subido ' . $numImages . ($numImages > 1 ? ' imagenes' : ' imagen')], 200);
            return $response;
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function deleteImageAction(Request $request, Fichaje $garment, $filename)
    {
        $image = new FichajeImage();
        $imageForm = $this->createForm(new FichajeImageType(), $image);

        if ($request->getMethod() === 'POST') {
            $uploadManager = $this->get('buscolook.upload.files');
            $uploadManager->deleteFile('prendas/' . $garment->getId() . '/' . $filename);
        }
        return $this->render('BuscolookAdminBundle:Bussiness:images.html.twig', array('form' => $imageForm->createView()));
    }

    public function createAction(Request $request, $bussinessId, $buscolooker = null)
    {
        $em = $this->getDoctrine()->getManager();

        $fichaje = new Fichaje();
        if (!is_null($bussinessId)) {
            $fichaje->setBussiness($em->getRepository('BuscolookWebBundle:Bussiness')->find($bussinessId));
        }
        if (!is_null($buscolooker)) {
            $fichaje->setBuscolooker($em->getRepository('BuscolookWebBundle:User')->find($buscolooker));
        }
        $fichaje->setName(uniqid());

        $em->persist($fichaje);
        $em->flush();

        return new JsonResponse(['fichajeId' => $fichaje->getId()], 200);
    }

    public function renderAction(Request $request, $fichajeId)
    {
        return $this->render('BuscolookWebBundle:Include:Popup/upload-image.html.twig', ['fichajeId' => $fichajeId]);
    }

    public function detailsAction(Request $request, Fichaje $fichaje)
    {
        return $this->render('BuscolookWebBundle:Fichaje:details.html.twig', [
            'fichaje' => $fichaje
        ]);
    }

    public function fichajePopupAction(Request $request, Fichaje $fichajeId = null, Bussiness $bussinessId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $garments = array();
        if ($fichajeId) {
            $garments = $em->getRepository('BuscolookWebBundle:Garment')->findBy(['fichaje' => $fichajeId]);
        }
        $form = $this->createForm(new ProductType($bussinessId), new Garment());

        return $this->render('BuscolookWebBundle:Fichaje:Include/popupFichaje.html.twig',
            [
                'fichajeId' => $fichajeId,
                'bussinessEntity' => $bussinessId,
                'form' => $form->createView(),
                'garments' => $garments,
                'entity' => 'bussiness'
            ]
        );
    }

    public function fichajePopupBuscolookerAction(Request $request, Fichaje $fichajeId = null, Bussiness $bussinessId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $garments = array();
        if ($fichajeId) {
            $garments = $em->getRepository('BuscolookWebBundle:Garment')->findBy(['fichaje' => $fichajeId]);
        }
        $form = $this->createForm(new ProductType($bussinessId), new Garment());

        return $this->render('BuscolookWebBundle:Fichaje:Include/popupFichaje.html.twig',
            [
                'fichajeId' => $fichajeId,
                'bussinessEntity' => $bussinessId,
                'form' => $form->createView(),
                'garments' => $garments,
                'entity' => 'buscolooker'
            ]
        );
    }

    public function addCommentAction(Request $request, Fichaje $fichaje)
    {
        $commentString = $request->request->get('comment', null);

        if (trim($commentString) === '') {
            return $this->redirect($this->generateUrl('buscolook_homepage'));
        }

        if (!$this->getUser()) {
            return $this->redirect($this->generateUrl('buscolook_homepage'));
        }

        $comment = new Comment();
        $comment->setFichaje($fichaje);
        $comment->setUser($this->getUser());
        $comment->setComment($commentString);
        if ($request->request->get('publish', null)) {
            $comment->publishInWall();
        }
        $fichaje->addComment($comment);

        $em = $this->getDoctrine()->getManager();
        $em->persist($fichaje);
        $em->flush();

        return $this->redirect(
            $this->generateUrl('buscolook_web_fichaje_details', [
                'fichaje' => $fichaje->getId()
            ])
        );
    }

    public function publishAction(Request $request, Fichaje $fichaje)
    {
        $em = $this->getDoctrine()->getManager();

        $fichajeType = $request->query->get('type');
        $fichajeRoute = $fichajeType == 'buscolooker' ? 'buscolook_web_buscolooker_public_profile' : 'buscolook_web_bussiness_public_profile';
        $fichaje->setPublished(true);
        $em->persist($fichaje);

        $em->flush();

        return $this->redirect(
            $this->generateUrl(
                $fichajeRoute,
                [
                    'user' => $this->get('security.context')->getToken()->getUser()->getId(),
                ]
            )
        );
    }

}
