<?php
namespace Buscolook\WebBundle\Manager;

use Buscolook\WebBundle\Entity\Bussiness;
use Symfony\Component\Security\Core\Encoder\Pbkdf2PasswordEncoder;
use Doctrine\ORM\EntityManager;

class UserManager
{
    private $em;
    const BUSSINESS = 1;
    const USER = 2;
    const AWS_ORIGIN = 1;
    const LOCAL_ORIGIN = 2;


    /**
    * Construct
    *
    */
    public function __construct(EntityManager $EntityManager)
    {
        $this->em = $EntityManager;
    }

    public function createUser($user, $role, $type = self::USER)
    {
        $user->setPassword($this->encodePassword($user->getPassword(), $user->getSalt()));
        $user->setUserRoles($this->em->getRepository('BuscolookWebBundle:Role')->findByRole($role));
        $this->em->persist($user);
        $this->em->flush();

        if ($type === self::BUSSINESS) {
            $bussiness = $this->createBussiness($user);
            $this->em->persist($bussiness);
            $this->em->flush();

            return $bussiness;
        }

        return $user;
    }

    public function createBussiness($user = null)
    {
        if ($user !== null) {
            $bussiness = new Bussiness();
            $bussiness->setName($user->getUsername());
            $bussiness->setEmail($user->getEmail());
            $bussiness->setBussinesstype($this->em->getRepository('BuscolookWebBundle:BussinessType')
                                        ->findOneBy(['type'=>'Tienda(s) Monomarca']));
            $bussiness->setUser($user);

            return $bussiness;
         }
    }

    protected function encodePassword($pass, $salt)
    {
        $passwordEncoder = new Pbkdf2PasswordEncoder();
        return $passwordEncoder->encodePassword($pass, $salt);
    }
}