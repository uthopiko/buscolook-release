<?php

namespace Buscolook\WebBundle\Form;

use Buscolook\AdminBundle\Form\RegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BussinessLogoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder->add('user', new RegistrationType());*/
        $builder
            ->add('logo', 'file', [
                'data_class' => null
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Bussiness'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_bussiness_logo_type';
    }
}
