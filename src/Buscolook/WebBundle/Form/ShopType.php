<?php
namespace Buscolook\WebBundle\Form;

use Buscolook\WebBundle\Form\EventListener\AddPasswordSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShopType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [ 'attr' => ['placeholder'=>'Nombre']])
            ->add('email', 'text', [ 'attr' => ['placeholder'=>'Email']])
            ->add('address', 'text', [ 'attr' => ['placeholder'=>'Dirección']])
            ->add('locality', 'text', [ 'attr' => ['placeholder'=>'Localidad'], 'mapped'=>false])
            ->add('description', 'textarea', [ 'attr' => ['placeholder'=>'Descripción']])
            ->add('cp', 'text', [ 'attr' => ['placeholder'=>'Código Postal']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Shop'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_user';
    }
}
