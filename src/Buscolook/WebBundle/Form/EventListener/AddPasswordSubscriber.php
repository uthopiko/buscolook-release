<?php
namespace Buscolook\WebBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AddPasswordSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SUBMIT => 'postSubmitData');
    }

    public function postSubmitData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        /*if (!$data || !$data->getId()) {
            $form->add('name', 'text');
        }*/
    }
}