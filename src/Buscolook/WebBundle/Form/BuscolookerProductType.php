<?php

namespace Buscolook\WebBundle\Form;

use Buscolook\AdminBundle\Form\RegistrationType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BuscolookerProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text',['attr'=>['placeholder'=>'NOMBRE DEL PRODUCTO']])
            ->add('fichaje_name', 'text',['mapped'=>false,'attr'=>['placeholder'=>'NOMBRE DEL FICHAJE']])
            ->add('description','textarea',['attr'=>['placeholder'=>'DESCRIPCIÓN DEL PRODUCTO']])
            ->add('bussiness', 'entity', ['class'=>'BuscolookWebBundle:Bussiness','property'=>'name','multiple'=>false,'mapped'=>false,'attr'=>['class'=>'principal-selector','data-url'=>'buscolook_bussiness_get_shops']])
            ->add('shops', 'entity', ['class'=>'BuscolookWebBundle:Shop','property'=>'name','multiple'=>true,'attr'=>['class'=>'secondary-selector']])
            ->add('subcategory', 'entity', ['class'=>'BuscolookWebBundle:SubCategory','property'=>'name'])
            ->add('brand', 'entity', ['class'=>'BuscolookWebBundle:Brand', 'property'=>'name'])
            ->add('price', 'text',['attr'=>['placeholder'=>'PRECIO']])
            ->add('urlWeb', 'text',['attr'=>['placeholder'=>'URL']]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Garment'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_garment';
    }
}
