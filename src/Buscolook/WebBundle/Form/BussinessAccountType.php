<?php

namespace Buscolook\WebBundle\Form;

use Buscolook\AdminBundle\Form\RegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BussinessAccountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder->add('user', new RegistrationType());*/
        $builder
            ->add('name', 'text', ['attr' => ['placeholder' => 'Nombre tienda']])
            ->add('contact', 'text', ['attr' => ['placeholder' => 'Persona Contacto']])
            ->add('phone', 'text', ['attr' => ['placeholder' => 'Teléfono']])
            ->add('web', 'text', ['attr' => ['placeholder' => 'Web']])
            ->add('tiendaonline')
            ->add('email', 'text', ['attr' => ['placeholder' => 'Email']])
            ->add('logo', 'file', [
                'data_class' => null
            ])
            ->add('description', 'textarea',['attr'=>['placeholder'=>'Introduce una dirección para tu comercio']])
            ->add('slogan', 'textarea', ['attr' => ['placeholder' => 'Introduce un slogan para tu tienda', 'class' => 'ffMedium']])
            ->add('rs1')
            ->add('rs2')
            ->add('rs3')
            ->add('rs4')
            ->add('rs5')
            ->add('showcase')
            ->add('outlet')
            ->add('status')
            ->add('appmovil')
            ->add('bussinesstype', 'entity', ['class' => 'BuscolookWebBundle:BussinessType', 'property' => 'type'])
            ->add('categories', 'entity', ['class' => 'BuscolookWebBundle:Category', 'property' => 'name', 'multiple' => true, 'expanded' => true])
            ->add('target');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Bussiness'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_bussiness';
    }
}
