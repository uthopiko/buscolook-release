<?php

namespace Buscolook\WebBundle\Form;

use Buscolook\AdminBundle\Form\RegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BussinessProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder->add('user', new RegistrationType());*/
        $builder
            ->add('name')
            ->add('contact')
            ->add('phone')
            ->add('web')
            ->add('tiendaonline')
            ->add('email')
            ->add('logo', 'file', [
                'data_class' => null
            ])
            ->add('description')
            ->add('rs1')
            ->add('rs2')
            ->add('rs3')
            ->add('rs4')
            ->add('rs5')
            ->add('showcase')
            ->add('outlet')
            ->add('status')
            ->add('appmovil')
            ->add('bussinesstype', 'entity', ['class'=>'BuscolookWebBundle:BussinessType', 'property'=>'type'])
            ->add('categories', 'entity', ['class'=>'BuscolookWebBundle:Category', 'property'=>'name', 'multiple'=>true])
            ->add('target')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Bussiness'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_bussiness';
    }
}
