<?php

namespace Buscolook\WebBundle\Form;

use Buscolook\AdminBundle\Form\RegistrationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BuscolookerAccountType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder->add('user', new RegistrationType());*/
        $builder
            ->add('username', 'text', ['attr' => ['placeholder' => 'Nombre Buscolooker']])
            ->add('email', 'text', ['attr' => ['placeholder' => 'Email Buscolooker']])
            ->add('urlblog', 'text', ['attr' => ['placeholder' => 'Website']]);
            /*->add('locality', 'text', ['attr' => ['placeholder' => 'Localidad']]);*/
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_bussiness';
    }
}
