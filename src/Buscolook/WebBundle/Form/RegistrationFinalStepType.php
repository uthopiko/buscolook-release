<?php
namespace Buscolook\WebBundle\Form;

use Buscolook\WebBundle\Form\EventListener\AddPasswordSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationFinalStepType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('locality', 'text', [ 'attr' => ['id' => 'locality','name'=>'locality','placeholder'=>'Ciudad'], 'mapped' => false])
            ->add('categories', 'entity', ['class'=>'BuscolookWebBundle:Category','expanded'=>true, 'property'=>'name', 'multiple'=>true]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Buscolook\WebBundle\Entity\Bussiness'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'buscolook_webbundle_user';
    }
}
