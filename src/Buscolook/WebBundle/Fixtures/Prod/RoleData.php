<?php

namespace Buscolook\WebBundle\Fixtures\Prod;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Buscolook\WebBundle\Entity\User;
use Buscolook\WebBundle\Entity\Role;

class RoleData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setRole('ROLE_USER');
        $manager->persist($role);
        $manager->flush();

        $role = new Role();
        $role->setRole('ROLE_BUSSINESS');
        $manager->persist($role);
        $manager->flush();

        $role = new Role();
        $role->setRole('ROLE_OPERATOR');
        $manager->persist($role);
        $manager->flush();

        $role = new Role();
        $role->setRole('ROLE_ADMIN');
        $manager->persist($role);
        $manager->flush();

        $role = new Role();
        $role->setRole('ROLE_PREMIUM');
        $manager->persist($role);
        $manager->flush();
    }
}