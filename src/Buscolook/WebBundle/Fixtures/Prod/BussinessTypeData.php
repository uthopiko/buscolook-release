<?php

namespace Buscolook\WebBundle\Fixtures\Prod;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Buscolook\WebBundle\Entity\BussinessType;

class BussinessTypeData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $bt = new BussinessType();
        $bt->setType('Tienda(s) Monomarca');
        $manager->persist($bt);
        $manager->flush();

        $bt = new BussinessType();
        $bt->setType('Tienda(s) Multimarca');
        $manager->persist($bt);
        $manager->flush();

        $bt = new BussinessType();
        $bt->setType('Tienda Online');
        $manager->persist($bt);
        $manager->flush();

    }
}