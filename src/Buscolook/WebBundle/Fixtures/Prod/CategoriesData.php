<?php

namespace Buscolook\WebBundle\Fixtures\Prod;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Buscolook\WebBundle\Entity\Category;

class CategoryData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $categories = ['Zapatos', 'Bolsos', 'Vestidos', 'Faldas', 'Pantalones / shorts', 'Cardigan / jerséis', 'Tops / camisetas', 'Camisas', 'Trajes / americanas', 'Abrigos / cazadoras', 'Ropa interior / baño', 'Accesorios / joyería'];

        foreach ($categories as $c) {
            $category = new Category();
            $category->setName($c);
            $category->setType(Category::ARTICLE);
            $category->setActive(true);
            $manager->persist($category);
        }

        $manager->flush();

    }
}