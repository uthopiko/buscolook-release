<?php

namespace Buscolook\WebBundle\Fixtures\Test;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Buscolook\WebBundle\Entity\Bussiness;

class LoadBussinessData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $bussiness = new Bussiness();
        $bussiness->setName('Empresa test behat');
        $bussiness->setEmail('Empresa test behat');

        $user->setEmail('asier+behat@buscolook.com');
        $user->setSalt('48tiqakiu484ossg0os4swwcw08sg0o');
        $user->setPassword('tGfV2srpDMTwjw1FM8Dkl+BzOZc5i9EtSIYEMCsVRKi9cLmaDV2vSg==');
        $user->setUsername('asier_test"');

        $user->addRole($manager->getRepository('BuscolookWebBundle:Role')->findOneByRole('ROLE_USER'));

        $manager->persist($user);
        $manager->flush();
    }
}