var BUSCOLOOK;

BUSCOLOOK = {

    mobile: false,

    init: function() {
        this.setup();
        this.menu.init();
        //this.filterMenu.init();
        this.galleries.init();
        this.tabs.init();
        this.profileTabs.init();
        this.buscolookers.init();
        this.likes.init();
    },

    setup: function() {
        var html = $("html");
        if(html.hasClass("mobile") || html.hasClass("ipod") || html.hasClass("ipad") || html.hasClass("iphone")) {
            this.mobile = true;
        }

        $(".inputSelectWrap select").customSelect({
            customClass: "inputSelect"
        });
    }
};

BUSCOLOOK.menu = {

    init: function() {
        this.menu = $("nav#menu");
        this.trigger = $("#header").find(".menuTrigger");

        this.markSubmenus();
        this.firstSubmenuHover();
        this.checkables.init();

        this.navLikeMenus();

        this.mobile();
    },

    markSubmenus: function() {
        this.menu.find("li").each(function() {
            var item = $(this);
            if (item.find("ul").length > 0) {
                item.addClass("hasChild");
                if (!item.find("> a").data("clickable")) {
                    item.find("> a").click(function(e) {
                        e.preventDefault();
                    });
                }
            }

            if (item.find("ul li").length == 1) {
                item.addClass("childHasOneItem");
            }
        });
    },

    firstSubmenuHover: function() {
        this.menu.find("li").hover(function() {
            var element = $(this);
            var parent = element.parent();
            element.addClass("hover");
//			parent.addClass("hover");
            if (element.index() == 0) {
                parent.addClass("firstChildHover");
            }
        }, function() {
            var element = $(this);
            var parent = element.parent();
            element.removeClass("hover");
            if (element.index() == 0) {
                parent.removeClass("firstChildHover");
            }
        });
    },

    navLikeMenus: function() {
        if ($(".navLikeMenu").length > 0 ) {
            $(".navLikeMenu > li").hover(function() {
                var element = $(this);
                var parent = element.parent();
                element.addClass("hover");
                if (element.index() == 0) {
                    parent.addClass("firstChildHover");
                }
            }, function() {
                var element = $(this);
                var parent = element.parent();
                element.removeClass("hover");
                if (element.index() == 0) {
                    parent.removeClass("firstChildHover");
                }
            });
        }
    },

    mobile: function() {
        var _this = this;
        this.trigger.on("click", function() {
            if (_this.menu.data("open")) {
                _this.menu.removeClass("open");
                _this.menu.data("open", false);
            } else {
                _this.menu.addClass("open");
                _this.menu.data("open", true);
            }
        });
    }

};

/*BUSCOLOOK.filterMenu = {
 init: function() {
 if($(".filterMenu").length > 0) {
 //$(".filterMenu").find(".by").hover(function() {
 $(".filterMenu").find("li").hover(function() {
 var element = $(this);
 //var parent = element.parent("li");
 var parent = element;
 var menu = parent.find(".navLikeMenu");
 parent.addClass("hover");
 }, function() {
 var element = $(this);
 //var parent = element.parent("li");
 var parent = element;
 var menu = parent.find(".navLikeMenu");
 parent.removeClass("hover");
 });

 }
 }
 };*/

BUSCOLOOK.galleries = {

    carousel_config: {
        items: 1,
        autoHeight: true,
        singleItem: true,
        pagination: false,
        responsive: true,
        autoPlay: false,
        lazyLoad: true,
        navigation: false
    },

    banner_carousel_config: {
        items: 1,
        autoHeight: true,
        singleItem: true,
        pagination: false,
        responsive: true,
        autoPlay: true,
        lazyLoad: false,
        navigation: false
    },

    init: function() {
        var _this = this;

        if(BUSCOLOOK.mobile) {
            this.carousel_config.autoPlay = false;
        }

        $(".owl-carousel").each(function() {
            var slides = $(this);
            var preview = slides.parents(".preview");
            var owl = slides.owlCarousel(_this.carousel_config);

            if (slides.find(".slide").length > 1 && preview) {
                var btn_prev = $('<span class="galleryPrev"></span>');
                var btn_next = $('<span class="galleryNext"></span>');
                btn_prev.on("click", function(e) {
                    owl.trigger("owl.prev");
                    e.stopPropagation();
                });
                btn_next.on("click", function(e) {
                    owl.trigger("owl.next");
                    e.stopPropagation();
                });

                if(slides.parents(".gallery").find(".like").length > 0) {
                    slides.append(btn_prev);
                    slides.append(btn_next);

                } else {
                    preview.append(btn_prev);
                    preview.append(btn_next);
                }
            }
        });

        this.hpBanner();
        this.bigGallery();

        if(!BUSCOLOOK.mobile) {
            this.clickThroughs();
        }
    },

    hpBanner: function() {
        var banner_slides = $(".banner-owl-carousel");
        var banner = banner_slides.parents(".banner");
        var banner_carousel = banner_slides.owlCarousel(this.banner_carousel_config);

        var btn_prev = $('<span class="galleryPrev"></span>');
        var btn_next = $('<span class="galleryNext"></span>');

        btn_prev.on("click", function(e) {
            banner_carousel.trigger("owl.prev");
            e.stopPropagation();
        });
        btn_next.on("click", function(e) {
            banner_carousel.trigger("owl.next");
            e.stopPropagation();
        });

        banner.append(btn_prev);
        banner.append(btn_next);
    },

    bigGallery: function() {


        var btn_prev = $('<span class="galleryPrev"></span>');
        var btn_next = $('<span class="galleryNext"></span>');

        var big_gallery_config = this.carousel_config;

        if($(".galleryThumbs").length > 0) {
            big_gallery_config.afterMove = after_move_function;
        }

        var carousel = $(".owl-carousel-big").owlCarousel(big_gallery_config);

        btn_prev.on("click", function(e) {
            carousel.trigger("owl.prev");
            e.stopPropagation();
        });
        btn_next.on("click", function(e) {
            carousel.trigger("owl.next");
            e.stopPropagation();
        });

        carousel.append(btn_prev);
        carousel.append(btn_next);

        if($(".galleryThumbs").length > 0) {
            var thumbs = $(".galleryThumbs");
            var owl_carousel = carousel.data("owlCarousel");
            thumbs.find("a").on("click", function(e) {
                var thumb = $(this);
                var slide = thumb.data("slide");
                owl_carousel.goTo(slide);
                e.preventDefault();
            });

            big_gallery_config.afterMove = function() {

            };

            function after_move_function() {
                var gallery_current_item = carousel.data("owlCarousel").currentItem;
                var thumb_item = thumbs.find("a[data-slide='"+gallery_current_item+"']");
                thumbs.find("a").removeClass("active");
                thumb_item.addClass("active");
            }
        }
    },

    clickThroughs: function() {
        $(".grid .item, .banner .item").each(function() {
            var item = $(this);
            item.find(".like").on("click", function(e) {
                e.stopPropagation();
            });
            if (item.find("*[data-href]")) {
                var href = item.find("*[data-href]").data("href");
                if (href) {
                    var clicker = item.find(".gallery");
                    clicker.addClass("clickable");
                    if (href=="same") {
                        href = item.find("a[data-href]").attr("href");
                    }
                    clicker.on("click", function(e) {
                        window.location = href;
                    });
                    if (item.find(".heading .subtitle").length > 0) {
                        var subtitle = item.find(".heading .subtitle");
                        subtitle.addClass("clickable");
                        subtitle.on("click", function(e) {
                            window.location = href;
                        });
                    }
                }
            }
        });
    }
};

BUSCOLOOK.menu.checkables = {

    init: function() {
        this._this = BUSCOLOOK.menu;
        this.onload();
        this.bind();
    },

    onload: function() {
        this._this.menu.find("a[data-checkable=true]").each(function() {
            var item = $(this);
            var parent = item.parent();
            if(item.data("checked")) {
                parent.addClass("checked");
                item.addClass("checked");
            }
        });
    },

    bind: function() {
        this._this.menu.find("a[data-checkable=true]").click(function(e) {
            var item = $(this);
            var parent = item.parent();
            if(item.data("checked")) {
                parent.removeClass("checked");
                item.removeClass("checked")
                item.data("checked", false);
            } else {
                /*parent.addClass("checked");
                item.addClass("checked")
                item.data("checked", true);*/
                window.location = item.attr('href');
            }
            e.preventDefault();
        });
    }
};

BUSCOLOOK.tabs = {

    init: function() {
        this.bind();
    },

    bind: function() {
        $(".tabs").each(function() {
            var container = $(this);
            var nav = container.find(".tabsNav");
            var links = nav.find("a");
            var tabs = container.find(".tab");

            links.on("click", function(e) {
                var link = $(this);
                var id = link.attr("href").replace("#", "");
                if($("#"+id).length > 0) {
                    var tab = $("#"+id);
                    links.removeClass("active");
                    link.addClass("active");
                    tabs.hide();
                    tab.fadeIn(500);
                    e.preventDefault();
                }
            });
        });
    }
};

BUSCOLOOK.profileTabs = {

    init: function() {
        this.bind();
    },

    bind: function() {
        $(".cf .item").each(function() {
            var container = $(this);

            container.on("click", function(e) {
                $('.tabsContent').css('display','none');
                $('#' + this.id + 'Content').css('display','block');
                $('.cf li').removeClass('active');
                container.addClass('active');
                e.preventDefault();
            });
        });
    }
};

BUSCOLOOK.buscolookers = {

    init: function() {
        $(".buscolookers .item .button.following, .profileContentHeader a.following, .profileContent a.following").each(function() {
            var button = $(this);
            var text_default = button.html();
            var text_alt = button.data("alt-text");

            button.hover(function() {
                button.html(text_alt);
            }, function() {
                button.html(text_default);
            });
        });
    }
};

BUSCOLOOK.likes = {

    init: function() {
        $(".icon-like").each(function() {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            //var Url = Routing.generate('buscolook_favourite_add_fichaje',{'garmentId':item});

            button.on("click", function(e) {
                e.preventDefault();
                $.ajax({
                    url: '#',
                    type: 'POST'
                }).done(function(data){
                    button.removeClass('icon-like');
                    button.addClass('icon-like-full');
                    var spanCount = button.next();
                    spanCount.text(parseInt(spanCount.text()) + 1);
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-full").each(function() {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_favourite_remove_garment',{'garmentId':item});

            button.on("click", function(e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'DELETE'
                }).done(function(data){
                    button.removeClass('icon-like-full');
                    button.addClass('icon-like');
                    var spanCount = button.next();
                    spanCount.text(parseInt(spanCount.text()) - 1);
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-black").each(function() {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_follower_add_shop',{'bussinessId':item});

            button.on("click", function(e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'POST'
                }).done(function(data){
                    button.removeClass('icon-like-black');
                    button.addClass('icon-like-black-full');
                    BUSCOLOOK.likes.init();
                });
            });
        });

        $(".icon-like-black-full").each(function() {
            var button = $(this);
            button.unbind();
            var item = button.data('item');

            var Url = Routing.generate('buscolook_follower_remove_shop',{'bussinessId':item});

            button.on("click", function(e) {
                e.preventDefault();
                $.ajax({
                    url: Url,
                    type: 'DELETE'
                }).done(function(data){
                    button.removeClass('icon-like-black-full');
                    button.addClass('icon-like-black');
                    BUSCOLOOK.likes.init();
                });
            });
        });
    }
};


$(document).ready(function () {
    BUSCOLOOK.init();
});