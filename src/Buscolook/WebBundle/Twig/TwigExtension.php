<?php
/**
 * Created by PhpStorm.
 * User: aramos
 * Date: 30/03/14
 * Time: 12:23
 */

namespace Buscolook\WebBundle\Twig;


use Buscolook\WebBundle\Manager\UserManager;

class TwigExtension extends \Twig_Extension {

    private $origin;

    public function __construct($origin) {
        $this->origin = $origin;
    }

    public function getFunctions()
    {
        return array(
            'form_categories_checks'   => new \Twig_Function_Node('Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode', array('is_safe' => array('html'))),
            'form_profile_errors'   => new \Twig_Function_Node('Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode', array('is_safe' => array('html'))),
            'custom_asset'   => new \Twig_Function_Method($this, 'customAsset')
        );
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('days_passed', [$this, 'daysDistance']),
        ];
    }

    public function customAsset($entity, $entityId, $thumbnailName, $asset) {
        if ($this->origin == UserManager::AWS_ORIGIN) {
            return 'https://buscolook.s3.amazonaws.com/' . $entity . '/' . $entityId . '/'.$thumbnailName . '/' . $asset;
        }
        else {
            return '/uploads/' . $entity . '/' . $entityId . '/'.$thumbnailName . '/' . $asset;
        }
    }

    /**
     * @param \DateTime $date
     * @return string
     */
    public function daysDistance(\DateTime $date) {
        return $date->diff(new \DateTime())->format("%a");
    }

    public function getName()
    {
        return 'buscolook_form_extension';
    }
} 