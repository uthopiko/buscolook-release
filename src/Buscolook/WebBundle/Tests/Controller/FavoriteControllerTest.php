<?php

namespace Buscolook\WebBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FavoriteControllerTest extends WebTestCase
{
    public function testShop()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/favoritos/tienda');
    }

    public function testAccount()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/mis-favoritos');
    }

    public function testAddfavourites()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/favourites/shops/{shopId}/user/{userId}');
    }

    public function testRemovefavourites()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/favourites/shops/{shopId}/user/{userId}');
    }

}
