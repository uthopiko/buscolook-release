<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aramos
 * Date: 10/16/13
 * Time: 9:59 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Buscolook\WebBundle\Utils;


class Tools
{

    /**
     * Obtains an object class name without namespaces
     */
    public function getRealClass($obj)
    {
        $classname = get_class($obj);

        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }

        return $classname;
    }

    /*
     * Get formated filters from entity
     */
    public function getFormatedFilters($entities, $route = 'buscolook_web_cotilleo'){
        $html = '';
        foreach ($entities as $e) {
            $html .= '<li><a href="" data-checkable="true">'. (isset($e['name']) ? $e['name'] : $e['locality']) .'</a></li>';
        }
        return $html;
    }

}