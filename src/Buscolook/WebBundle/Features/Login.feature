#language: en

Feature: Login in application

I want to log in applicaction

@login
Scenario: Login action create
    When I go to "/login"
    Then the response status code should be 200

@login
Scenario: Log correctly with user
    When I go to "/login"
    And I fill in "username" with "asier+behat@buscolook.com"
    And I fill in "password" with "asier"
    And I press "checkLogin"
    Then I should be on the homepage
    And in session is authenticated with "ROLE_USER"

@login
Scenario: Log incorrectly
    When I go to "/login"
    And I fill in "username" with "asier+user@busolook.com"
    And I fill in "password" with "rootpas"
    And I press "checkLogin"
    Then I should be on "/login"
    And in session is not authenticated with "ROLE_USER"