#language: en

Feature: Edit profile

I want to manage my profile

@profile
Scenario: show your profile
    Given I am logged in as User
    When I go to "/user/account"
    Then the response status code should be 200
    And the "username" field should contain "asier_test" 
    And the "email" field should contain "asier+behat@buscolook.com"

@profile
Scenario: edit your profile
    Given I am logged in as User
    When I go to "/profile/account" 
    When I fill in the following:
            | email        | asier2@buscolook.com |
            | password     | bar         |
            | verification | bar         |
    And I press "save"
    Then the response status code should be 200
    And I should see "Perfil actualizado"
    And the fields in database should be
            | email        | asier2@buscolook.com |
            | password     | bar         |

@profile
Scenario: Retrieve notifications from favourite shops
    Given I am logged in as User
    And I mark as favourite "empresa_prueba" 
    And "empresa" create new garment
    When I go to "/profile"
    Then should be notifications from "empresa"