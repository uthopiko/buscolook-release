#language: en

Feature: Register in app

I want to register in app as user

@register
Scenario: Register correctly
    When I go to "/register"
    And I fill in "buscolook_webbundle_user_username" with "asierbehat"
    And I fill in "buscolook_webbundle_user_password_first" with "rootpass"
    And I fill in "buscolook_webbundle_user_password_second" with "rootpass"
    And I fill in "buscolook_webbundle_user_email" with "asier+behat@buscolook.com"
    And I press "register"
    Then "email" "asier+behat@buscolook.com" should be created in "user"
    And in session is authenticated with "ROLE_USER"

@register
Scenario: Register incorrectly
    When I go to "/register"
    And I fill in "buscolook_webbundle_user_username" with "as"
    And I fill in "buscolook_webbundle_user_password_first" with "rootpass"
    And I fill in "buscolook_webbundle_user_password_second" with "rootpass"
    And I fill in "buscolook_webbundle_user_email" with "asier+behatbuscolook.com"
    And I press "register"
    Then I should be on "/register"
    And I should see "This value is too short"
    And I should see "Correo electrónico no válido"
    And "username" "asierbehat_incorrec" should not be created in "user"
    And in session is not authenticated with "ROLE_USER"

@register
Scenario: Register bussiness correctly
    When I go to "/register/bussiness"
    And I fill in "buscolook_webbundle_user_username" with "asierbehat"
    And I fill in "buscolook_webbundle_user_password_first" with "rootpass"
    And I fill in "buscolook_webbundle_user_password_second" with "rootpass"
    And I fill in "buscolook_webbundle_user_email" with "asier+behatbussiness@buscolook.com"
    And I press "register"
    Then I should see "Empresa registrada correctamente"
    And "email" "asier+behatbussiness@buscolook.com" should be created in "user"
    And in session is authenticated with "ROLE_BUSSINESS"

@register
Scenario: Register bussiness incorrectly
    When I go to "/register/bussiness"
    And I fill in "buscolook_webbundle_user_username" with "as"
    And I fill in "buscolook_webbundle_user_password_first" with "rootpass"
    And I fill in "buscolook_webbundle_user_password_second" with "rootpass"
    And I fill in "buscolook_webbundle_user_email" with "asier+behatbuscolook.com"
    And I press "register"
    Then I should be on "/register/bussiness"
    And I should see "This value is too short"
    And I should see "Correo electrónico no válido"
    And "email" "asier+behatbuscolook.com" should not be created in "user"
    And in session is not authenticated with "ROLE_USER"

