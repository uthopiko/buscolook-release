#language: en

Feature: Aplication Home

I want to show my aplication home

Scenario: I am on the homepage
    When I go to "/"
    Then the response status code should be 200

Scenario: Show Home
    When I go to "/"
    Then I should be on the homepage
    And I should see 2 "<div class=\"slide\">" elements