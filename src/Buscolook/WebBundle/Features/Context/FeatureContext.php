<?php

namespace Buscolook\WebBundle\Features\Context;

use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext,
    Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

use Behat\Behat\Context\BehatContext,
    Behat\Behat\Context\Step,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;
use Buscolook\WebBundle\Fixtures\Test\LoadUserData,
    Buscolook\WebBundle\Fixtures\Prod\RoleData,
    Doctrine\Common\DataFixtures\Loader,
    Doctrine\Common\DataFixtures\Executor\ORMExecutor,
    Doctrine\Common\DataFixtures\Purger\ORMPurger;

//
// Require 3rd-party libraries here:
//
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareInterface
{
    private $kernel;
    private $parameters;
    private $mink;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {

        $this->useContext(
            'redirect',
            new \Behat\CommonContexts\MinkRedirectContext()
        );
        $this->useContext(
            'data',
            new DataContext()
        );
        $this->useContext(
            'web',
            new WebContext()
        );
        $this->parameters = $parameters;
    }

    /**
     * @BeforeScenario @register
     */
    public function cleanDatabaseFixtures()
    {
        $this->persistFixtures();
    }

    /**
     * @BeforeScenario @login
     * @BeforeScenario @profile
     */
    public function loadLoginFixtures()
    {
        $loader = new Loader();
        $loader->addFixture(new RoleData);
        $loader->addFixture(new LoadUserData);
        $this->persistFixtures($loader);
    }

    private function persistFixtures($loader = null)
    {
        $purger = new ORMPurger();
        if ($loader == null) {
            $loader = new Loader();
            $loader->addFixture(new RoleData);
        }
        $executor = new ORMExecutor($this->getManager(), $purger);
        $executor->execute($loader->getFixtures());
    }


    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;

    }

    /**
     * @Given /^(?:|I )am logged in as User$/
     */
    public function iAmLoggedInAsUser()
    {
        return array(
            new Step\When('I go to "/login"'),
            new Step\When('I fill in "username" with "asier+behat@buscolook.com"'),
            new Step\When('I fill in "password" with "asier"'),
            new Step\When('I press "checkLogin"'),
            new Step\Then('I should be on the homepage'),
            new Step\Then('in session is authenticated with "ROLE_USER"')
        );
    }

    /**
     * @Then /^in session is authenticated with "(?P<role>(?:[^"]|\\")*)"$/
     */
    public function inSessionIsAuthenticated($role)
    {
        $user = $this->kernel->getContainer()->get('security.context')->getToken()->getUser();
        assertTrue(is_object($user));
        $exist = false;
        foreach ($user->getRoles() as $r) {
            if ($r->getRole() == $role) {
                $exist = true;
            }
        }
        assertTrue($exist);
    }

    /**
     * @Then /^in session is not authenticated with "(?P<role>(?:[^"]|\\")*)"$/
     */
    public function inSessionIsNotAuthenticated($role)
    {
        $user = $this->kernel->getContainer()->get('security.context')->getToken()->getUser();
        assertFalse(is_object($user));
    }

    /**
     * @Then /^"(?P<field>(?:[^"]|\\")*)" "(?P<value>(?:[^"]|\\")*)" should be created in "(?P<table>(?:[^"]|\\")*)"$/
     */
    public function valueShouldBeCreatedInDatabase($field, $value, $table)
    {
        $method = 'findOneBy' . ucfirst($field);
        $entity = $this->getRepository($table)->$method($value);
        assertTrue($entity != null);
        $method = 'get' . ucfirst($field);
        assertTrue($entity->$method() === $value);
    }

    /**
     * @Given /^"(?P<field>(?:[^"]|\\")*)" "(?P<value>(?:[^"]|\\")*)" should not be created in "(?P<table>(?:[^"]|\\")*)"$/
     */
    public function userShouldNotBeCreatedInDatabase($field, $value, $table)
    {
        $method = 'findOneBy' . ucfirst($field);
        $entity = $this->getRepository($table)->$method($value);
        assertTrue($entity === null);
    }

    /**
     * @Given /^in session is not be authenticated$/
     */
    public function inSessionIsNotBeAuthenticated()
    {
        throw new PendingException();
    }

    /**
     * @Given /^I am authenticated with "([^"]*)" and "([^"]*)"$/
     */
    public function iAmAuthenticated($name, $pass)
    {
        throw new PendingException();
    }


    protected function generateUrl($url, $parameters = [], $full = true)
    {
        return $this->kernel->getContainer()->get('router')->generate($url, $parameters, $full);
    }

    /**
     * Returns the Doctrine entity manager.
     *
     * @return Doctrine\ORM\EntityManager
     */
    protected function getManager()
    {
        return $this->kernel->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Returns the Doctrine repository manager for a given entity.
     *
     * @param string $entityName The name of the entity.
     *
     * @return Doctrine\ORM\EntityRepository
     */
    protected function getRepository($entityName)
    {
        return $this->getManager()->getRepository('BuscolookWebBundle:' . $entityName);
    }

}
