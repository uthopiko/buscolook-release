#language: en

Feature: Bussiness

I want to manage shop

Scenario: show your profile
    Given I am authenticated with "asier+empresa@buscolook.com" and "pass"
    When I go to "/bussiness/profile" 
    Then the response status code should be 200
    And the "username" field should contain "asier_test" 
    And the "email" field should contain "asier@buscolook.com" 

Scenario: edit your bussiness profile (Central Shop)
    Given I am authenticated with "asier+empresa@buscolook.com" and "pass"
    When I go to "/bussiness/profile" 
    And I fill in the following:
            | buscolook_webbundle_bussiness_name    | Empresa test edit |
            | buscolook_webbundle_bussiness_email   | empresa_test_edit@buscolook.com |
            | buscolook_webbundle_bussiness_address | adress_fake_for_edit |
            | buscolook_webbundle_bussiness_phone   | 666555444 |
            | buscolook_webbundle_bussiness_schedule| tarde edit |
    And I attach the file "image_test.jpg" to "image"
    And I press "save"
    Then the response status code should be 200
    And I should see "Datos Actualizados"
    And the fields in database should be
            | name    | Empresa test edit |
            | email   | empresa_test_edit@buscolook.com |
            | address | adress_fake_for_edit |
            | phone   | 666555444 |
            | schedule| tarde edit |

Scenario: Add new Shop
    Given I am authenticated with "asier+empresa@buscolook.com" and "pass"
    And I am on "/bussiness/profile" 
    When I press "new_shop"
    And I go to "/shop/add"
    And I fill in the following:
            | buscolook_webbundle_shop_name    | Tienda add test |
            | buscolook_webbundle_shop_email   | shop_test_edit@buscolook.com |
            | buscolook_webbundle_shop_address | shop_adress_fake_for_edit |
            | buscolook_webbundle_shop_phone   | 666555444 |
            | buscolook_webbundle_shop_schedule| tarde edit |
    And I press "add_shop"
    Then I should see "Tienda Añadida"
    And I should be on "/bussiness/profile"
    And the fields in database should be
            | name    | Tienda add test |
            | email   | shop_test_edit@buscolook.com |
            | address | shop_adress_fake_for_edit |
            | phone   | 666555444 |
            | schedule| tarde edit |